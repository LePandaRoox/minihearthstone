import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})

/**
 * Main menu component
 */
export class MainMenuComponent implements OnInit {


  /**
   * Default constructor
   */
  constructor() { }

  /**
   * Component's lifecycle hook
   */
  ngOnInit() {
  }

}
