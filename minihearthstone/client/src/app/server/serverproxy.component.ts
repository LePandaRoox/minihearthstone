import { Component } from '@angular/core';
import { Server } from './server';

// Web socket imports
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { Router } from '@angular/router';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import * as FacadeProperties from '../webclientfacade/facade-properties';

import * as $ from 'jquery';

@Component({
  selector: 'app-path-location',
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],
  template: `
    <h1>PathLocationStrategy</h1>
    Current URL is: <code>{{location.path()}}</code><br>
    Normalize: <code>/foo/bar/</code> is: <code>{{location.normalize('foo/bar')}}</code><br>
  `
})

/**
 * Server proxy component
 */
export class ServerProxyComponent implements Server {

  serverURL: string;
  private stompClient;
  private playerId;
  router: Router;

  /**
   * ServerProxyComponent constructor
   * @param router change url route to switch components without reloading the page
   */
  constructor(router: Router) {

    // Set router
    this.router = router;

    // Set server URL
    this.serverURL = 'http://localhost:8090/';

    // Generate unique playerId
    const date = new Date();
    const time = date.getTime();
    this.playerId = Math.floor(Math.random() * 1000) + 1 + '' + time;

    // Connect websocket to server
    this.connect(this.router);
  }

  /**
   * Connect web socket to server
   * @param router router (used to switch between components)
   */
  connect(router: Router) {

    const playerId = this.playerId;

    // Configure the web socket
    const ws = new SockJS(this.serverURL);
    this.stompClient = Stomp.over(ws);
    const that = this;

    // Send connection
    this.stompClient.connect({}, function (frame) {

      // Subscribe to url : /client/{playerId}
      that.stompClient.subscribe('/client/' + playerId, (response) => {
        if (response.body) {
          console.log(response.body);
        }
      });

      // Receive match request from the other player
      that.stompClient.subscribe('/client/sendmatchrequest/player/' + playerId, (response) => {
        if (response.body) {
          const opponent = JSON.parse(response.body);
          console.log('ok');
          FacadeProperties.setPlayerOpponent(opponent);
          if (FacadeProperties.playerOpponent != null) {
            $('#serverMessage').html('<p>Match found : ' + FacadeProperties.playerOpponent.name + '</p>');
            $('#acceptMatchRequestBtn').show();
            $('#rejectMatchBtn').hide();
            $('#matchProposalModal').show();
          }
        }
      });

      // Receive match request rejection from the other player
      that.stompClient.subscribe('/client/rejectmatchrequest/player/' + playerId, (response) => {
        if (response.body) {
          $('#serverMessage').html('<p>' + response.body + '</p>');
          $('#acceptMatchBtn').hide();
          $('#acceptMatchRequestBtn').hide();
          $('#rejectMatchBtn').hide();
          $('#okBtn').show();
          $('#matchProposalModal').show();
          FacadeProperties.setPlayerOpponent(null);
        }
      });

      // Receive match request acceptation from the other player
      that.stompClient.subscribe('/client/acceptmatchrequest/player/' + playerId, (response) => {
        if (response.body) {

          // Parse to JSON
          const match = JSON.parse(response.body);

          // Inform player that opponent has accepted the match
          $('#serverMessage').html('<p>' + FacadeProperties.playerOpponent.name + ' has accepting your challenge. </p>' +
            'Redirecting to game lobby ...');
          $('#matchProposalModal').show();

          // Get all heroes
          that.getAllHeroes();

          // Save gameId in properties
          FacadeProperties.setGameId(match.id);

          // Redirect to game lobby using match id and player id
          setTimeout(function () {
            console.log('redirect from connect subscribe');
            // router.navigate(['/game/' + match.id + '/lobby/' + playerId]);
            router.navigate(['/lobby']);
          }, 1600);
        }
      });

      // Receive notification that other player has chosen their hero and that game can start
      that.stompClient.subscribe('/client/haschosenhero/player/' + playerId, (response) => {
        if (response.body) {

          // Parse to JSON
          const match = JSON.parse(response.body);

          // Get other player's hero to save in properties


          // If match contains both players id then send init game
          if (match.p1ClientId !== null && match.p2ClientId !== null) {
            setTimeout(function () {
              that.initGame();
            }, 500);
          }
        }
      });


      // Receive other player's hero
      that.stompClient.subscribe('/client/receivehero/player/' + playerId, (response) => {
        if (response.body) {
          const hero = JSON.parse(response.body);
          FacadeProperties.setOpponentHero(hero);
          console.log('Init game ennemy hero : ' + FacadeProperties.opponentHero.name);
        }
      });

      // Receive notification that other player has logged out
      that.stompClient.subscribe('/client/receivenotifylogout/player/' + playerId, (response) => {
        if (response.body) {

          // Show message in modal
          $('#serverNotificationMessage').html(response.body);
          $('#generalNotificationModal').show();

          FacadeProperties.setPlayerOpponent(null);

          // Redirect to dashboard
          setTimeout(function () {
            console.log('Redirect to dashboard');
            router.navigate(['/dashboard']);
            $('#generalNotificationModal').hide();
          }, 1600);
        }
      });

      // Receive gameboard update from other player
      that.stompClient.subscribe('/client/receivegameboardupdate/player/' + playerId, (response) => {
        if (response.body) {

          // Update gameboard in properties
          const gameBoard = JSON.parse(response.body);
          FacadeProperties.setGameBoard(gameBoard);

          // Indicate that gameboard update from server has been received by setting reception to true
          console.log('Received gameboard update from other player');
          FacadeProperties.setReception(true);
        }
      });

      // Receive end of turn signal from other player
      that.stompClient.subscribe('/client/receiveendofturn/player/' + playerId, (response) => {
        if (response.body) {

          // Update gameboard in properties
          const gameBoard = JSON.parse(response.body);
          FacadeProperties.setGameBoard(gameBoard);

          // Notify player that turn has been ended
          $('#serverMessageGameBoard').html('It\'s now your turn!');
          $('#gameboardMessagesModal').show();

          // Indicate that gameboard update from server has been received by setting reception to true
          console.log('Received gameboard update from other player');
          FacadeProperties.setReception(true);

        }
      });

    });
  }

  /**
   * Login with username to server (user will be saved in JPAPersistence until disconnection)
   * @param playername Player name entered in login interface
   */
  login(playername) {
    const playerId = this.playerId;
    let saved = true;

    // Subscribe to url: /client/login/player/{playerId}
    this.stompClient.subscribe('/client/login/player/' + playerId, (response) => {
      if (response.body) {
        const player = JSON.parse(response.body);

        if (player.clientId !== -1) {
          // Store player info in FacadeProperties
          FacadeProperties.setPlayer(player);

          // Switch to waiting room
          this.router.navigate(['/dashboard']);
          saved = true;
        } else {
          console.log('Please choose another username');
        }
      }
    });
    saved = false;
    this.stompClient.send('/server/login/player/' + playerId + '/playername/' + playername);
    return saved;
  }

  /**
   * Send player logout to server
   */
  logout() {
    const playerId = this.playerId;

    // If current in lobby or in-game send a message to the other player that i've logged out
    if (FacadeProperties.playerOpponent != null) {
      const opponentId = FacadeProperties.playerOpponent.clientId;
      this.stompClient.send('/server/notifylogout/player/' + playerId + '/opponent/' + opponentId);
    }
    // Subscribe to url: /client/removeplayer/player/{playerId}
    this.stompClient.subscribe('/client/logout/player/' + playerId, (response) => {
      if (response.body) {

        setTimeout(function () {
          // Reload page, reinitialized web socket connection
          window.location.href = '/';
        }, 1000);
      }
    });
    this.stompClient.send('/server/logout/player/' + playerId);

  }

  /**
   * Send request to server to find a player match
   */
  getMatch() {
    const playerId = this.playerId;

    // Subscribe to url: /client/lookingformatch/player/{playerId}
    this.stompClient.subscribe('/client/lookingformatch/player/' + playerId, (response) => {
      if (response.body) {
        const size = JSON.parse(response.body);
        if (size === 1) {
          // show fenetre vous etes le seul joueur a vouloir jouer
        }
      }
    });
    this.stompClient.send('/server/lookingformatch/player/' + playerId);

    // Subscribe to url: /client/requestmatch/player/{playerId}
    this.stompClient.subscribe('/client/requestmatch/player/' + playerId, (response) => {
      if (response.body) {
        const opponent = JSON.parse(response.body);
        FacadeProperties.setPlayerOpponent(opponent);
        if (FacadeProperties.playerOpponent != null) {
          $('#serverMessage').html('<p>Match found : ' + FacadeProperties.playerOpponent.name + '</p>');
          $('#acceptMatchBtn').show();
          $('#rejectMatchBtn').show();
          $('#matchProposalModal').show();
        }
      }
    });
    this.stompClient.send('/server/requestmatch/player/' + playerId);
  }

  /**
   * Accept match proposal from server initiated by me
   */
  acceptMatch() {
    const that = this;
    const playerId = this.playerId;

    if (FacadeProperties.playerOpponent != null) {
      const player2Id = FacadeProperties.playerOpponent.clientId;

      // Subscribe to url: /client/acceptmatch/player/{player1Id}
      this.stompClient.subscribe('/client/acceptmatch/player/' + playerId, (response) => {
        if (response.body) {
          const match = JSON.parse(response.body);
        }
      });

      // Send accept match
      this.stompClient.send('/server/acceptmatch/player/' + playerId + '/opponent/' + player2Id);

      // Send match request to other player
      this.stompClient.send('/server/sendmatchrequest/player/' + playerId + '/opponent/' + player2Id);
    }
  }

  /**
   * Send accept match request initiated from another player to server
   */
  acceptMatchRequest() {
    const playerId = this.playerId;

    if (FacadeProperties.playerOpponent != null) {
      const player2Id = FacadeProperties.playerOpponent.clientId;

      // Subscribe to url: /client/acceptmatch/player/{player1Id}
      this.stompClient.subscribe('/client/acceptmatch/player/' + playerId, (response) => {
        if (response.body) {
          const match = JSON.parse(response.body);
          if (match.p1Answer === 'accept' && match.p2Answer === 'accept') {
            $('#serverMessage').html('<p>Redirecting to lobby ...</p>');
            $('#acceptMatchBtn').hide();
            $('#acceptMatchRequestBtn').hide();
            $('#rejectMatchBtn').hide();
            $('#okBtn').hide();
            $('#matchProposalModal').show();

            const router = this.router;

            // Get all heroes
            this.getAllHeroes();

            // Save gameId in properties
            FacadeProperties.setGameId(match.id);

            // Redirect to game lobby using match id and player id
            setTimeout(function () {
              // router.navigate(['/game/' + match.id + '/lobby/' + playerId]);
              router.navigate(['/lobby']);
            }, 1600);
          }
        }
      });

      // Send accept match to other player
      this.stompClient.send('/server/acceptmatch/player/' + playerId + '/opponent/' + player2Id);

      // Let other player know that match has been accepted
      this.stompClient.send('/server/acceptmatchrequest/player/' + playerId + '/opponent/' + player2Id);

    }
  }

  /**
   * Reject match proposal from server
   */
  rejectMatch() {
    const playerId = this.playerId;

    // Subscribe to url: /client/rejectmatch/player/{player1Id}
    this.stompClient.subscribe('/client/rejectmatch/player/' + playerId, (response) => {
      if (response.body) {
        $('#matchProposalModal').hide();
      }
    });

    // Send reject match
    this.stompClient.send('/server/rejectmatch/player/' + playerId);

    // Notify other player that his match request has been rejected
    if (FacadeProperties.playerOpponent != null) {
      const opponentClientId = FacadeProperties.playerOpponent.clientId;
      this.stompClient.send('/server/rejectmatchrequest/player/' + playerId + '/opponent/' + opponentClientId);
      FacadeProperties.setPlayerOpponent(null);
    }
  }

  /**
   * Get player object from serveur using playerId
   */
  getPlayer() {
    const playerId = this.playerId;

    // Subscribe to url: /client/playerinfo/player/{playerId}
    this.stompClient.subscribe('/client/playerinfo/player/' + playerId, (response) => {
      if (response.body) {
        const player = JSON.parse(response.body);
        FacadeProperties.setPlayer(player);
      }
    });

    this.stompClient.send('/server/playerinfo/player/' + playerId);
  }

  /**
   * Get all players from server
   */
  getAllPlayers() {
    const playerId = this.playerId;

    // Subscribe to url: /client/getallplayers/player/{playerId}
    this.stompClient.subscribe('/client/getallplayers/player/' + playerId, (response) => {
      if (response.body) { }
    });

    this.stompClient.send('/server/getallplayers/player/' + playerId);
  }

  /**
   * Get the list of all heroes from server
   */
  getAllHeroes() {
    const playerId = this.playerId;

    // Receive list of all heroes sent from server
    this.stompClient.subscribe('/client/getallheroes/player/' + playerId, (response) => {
      if (response.body) {
        const heroes = JSON.parse(response.body);
        FacadeProperties.setHeroes(heroes);
        console.log('Stored in client : ' + FacadeProperties.heroes);
      }
    });

    this.stompClient.send('/server/getallheroes/player/' + playerId);
  }

  /**
   * Send hero choice to server
   * @param heroId id of selected hero
   */
  chooseHero(heroId: number) {
    const playerId = this.playerId;

    // Subscribe to url: /client/choosehero/player/{playerId}
    this.stompClient.subscribe('/client/choosehero/player/' + playerId, (response) => {
      if (response.body) {
        const match = JSON.parse(response.body);

        // Notify player of server response
        $('#serverMessageForHeroChoice').html('You\'ve chosen ' +
          FacadeProperties.hero.name +
          '. Waiting for other player to choose their hero ...');
        $('#gameStartNotificationModal').show();


        // If match contains both players id then send init game
        if (match.p1ClientId !== null && match.p2ClientId !== null) {
          this.initGame();
          console.log('From choose hero subscribe');
        }

        const opponentId = FacadeProperties.playerOpponent.clientId;

        // Send my hero to opponent
        this.stompClient.send('/server/sendmyhero/player/' + this.playerId + '/opponent/' + opponentId);

        // Else notify other player that I have chosen my hero
        this.stompClient.send('/server/haschosenhero/player/' + opponentId + '/game/' + FacadeProperties.gameId);
      }
    });

    if (FacadeProperties.gameId !== null && FacadeProperties.playerOpponent != null) {
      this.stompClient.send('/server/choosehero/player/' + playerId + '/hero/' + heroId +
        '/game/' + FacadeProperties.gameId);
    }
  }

  /**
   * Get the hero of opponent player from server
   * @param opponentId id of opponent player
   */
  getOpponentHero(opponentId: number) {

    // Get hero of opponent player
    this.stompClient.subscribe('/client/gethero/player/' + this.playerId, (response) => {
      if (response.body) {
        const hero = JSON.parse(response.body);
        FacadeProperties.setOpponentHero(hero);
        console.log('Init game ennemy hero : ' + FacadeProperties.opponentHero.name);
      }
    });

    this.stompClient.send('/server/gethero/player/' + this.playerId + '/opponent/' + opponentId);
  }


  /**
   * Get game initializatio from server
   */
  initGame() {

    // Receive game initialize
    this.stompClient.subscribe('/client/initgame/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameBoard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameBoard);
        console.log('Receive game initialization');
        this.getFirstHandOfCards();
      }
    });

    // Ask for game initialisation
    this.stompClient.send('/server/initgame/player/' + this.playerId + '/game/' + FacadeProperties.gameId);

    // Redirect to gameboard
    const router = this.router;
    setTimeout(function () {
      // router.navigate(['/game/' + FacadeProperties.gameId]);
      router.navigate(['/gameboard']);
    }, 1600);
  }

  /**
   * Get the card deck of my hero from server
   */
  getFirstHandOfCards() {
    const playerId = this.playerId;

    // Receive the card deck from
    this.stompClient.subscribe('/client/getfirsthand/player/' + playerId, (response) => {
      if (response.body) {
        /*const cards = JSON.parse(response.body);
        FacadeProperties.setHandOfCards(cards);
        console.log('Stored in client : ' + FacadeProperties.handOfCards);*/
        const gameBoard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameBoard);
        console.log('Stored gameboard in client' + FacadeProperties.gameBoard.id);
      }
    });
    const heroId = FacadeProperties.hero.id;
    const gameId = FacadeProperties.gameId;
    this.stompClient.send('/server/getfirsthand/player/' + playerId + '/hero/' + heroId + '/game/' + gameId);
  }

  /**
   * Send card choice to server
   * @param cardUniqueId  unique id of chosen card
   */
  chooseCard(cardUniqueId: string) {
    const playerId = this.playerId;

    // Receive the card deck from
    this.stompClient.subscribe('/client/choosecard/player/' + playerId, (response) => {
      if (response.body) {

        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();

      }
    });

    // Send chosen card to server
    const heroId = FacadeProperties.hero.id;
    const gameId = FacadeProperties.gameId;
    this.stompClient.send('/server/choosecard/player/' + playerId + '/game/' + gameId, {}, cardUniqueId);
  }

  /**
   * Send request to server to apply my card's effect on target
   * @param myCard my card unique id
   * @param opponentCard opponent card unique id
   */
  applyCardOnTargetServant(myCardUniqueId: string, opponentCardUniqueId: string) {

    console.log('Called applyCardOnTargetServant()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/cardattackservant/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });
    this.stompClient.send('/server/cardattackservant/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId, {},
      JSON.stringify([myCardUniqueId, opponentCardUniqueId])
    );
  }

  /**
   * Send request to server to apply my spell card on servant
   * @param myTargetSpellCardUniqueId  my spell card's unique id
   * @param opponentCardUniqueId  my opponent's card unique id
   */
  applySpellCardOnServant(myTargetSpellCardUniqueId: string, opponentCardUniqueId: string) {

    console.log('Called applySpellCardOnServant()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/spellcardattackservant/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });
    this.stompClient.send('/server/spellcardattackservant/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId, {},
      JSON.stringify([myTargetSpellCardUniqueId, opponentCardUniqueId])
    );
  }

  /**
   * Send request to server to apply my spell card on friendly servant
   * @param myTargetSpellCardUniqueId  my spell card's unique id
   * @param friendlyCardUniqueId my friendly servant's card unique id
   */
  applySpellCardOnFriendlyServant(myTargetSpellCardUniqueId: string, friendlyCardUniqueId: string) {

    console.log('Called applySpellCardOnFriendlyServant()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/spellcardonfriendlyservant/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });
    this.stompClient.send('/server/spellcardonfriendlyservant/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId, {},
      JSON.stringify([myTargetSpellCardUniqueId, friendlyCardUniqueId])
    );
  }

  /**
   * Send request to server to attack target hero
   * @param myCardUniqueId my servant card unique id
   */
  applyCardOnTargetHero(myCardUniqueId: string) {
    console.log('Called applyCardOnTargetServant()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/cardattackhero/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });
    this.stompClient.send('/server/cardattackhero/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId,
    {}, myCardUniqueId);
  }

  /**
   * Send resquest to server to apply my spell card on opponent hero
   * @param myTargetSpellCardUniqueId my spell card's unique id
   */
  applySpellCardOnHero(myTargetSpellCardUniqueId: string) {
    console.log('Called applySpellCardOnHero()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/spellcardattackhero/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });
    this.stompClient.send('/server/spellcardattackhero/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId,
    {}, myTargetSpellCardUniqueId);

  }

  /**
   * Send request to apply hero special on servant
   * @param opponentServantUniqueId opponent servant card unique id
   */
  applyHeroSpecialOnServant(opponentServantUniqueId: string) {
    console.log('Called applyHeroSpecialOnServant()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/heroattackservant/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });

    this.stompClient.send('/server/heroattackservant/player/' + this.playerId + '/opponent/' + opponentId + '/game/' + gameId,
    {}, opponentServantUniqueId);
  }

  /**
   * Send request to apply my hero special on opponent's hero
   */
  applyHeroSpecialOnHero() {
    console.log('Called applyHeroSpecialOnHero()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/heroattackhero/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });

    this.stompClient.send('/server/heroattackhero/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId, {});
  }

  /**
   * Send request to apply my hero special skill on self
   */
  useHeroSpecial() {
    console.log('Called useHeroSpecial()');

    const gameId = FacadeProperties.gameId;
    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Get hero of opponent player
    this.stompClient.subscribe('/client/useherospecial/player/' + this.playerId, (response) => {
      if (response.body) {
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);

        // Update my gameboard
        this.updateGameBoard();
      }
    });

    this.stompClient.send('/server/useherospecial/player/' + this.playerId + '/opponent/' + opponentId + ' /game/' + gameId, {});
  }

  /**
   * Send request to server to get updated gameboard
   */
  updateGameBoard() {
    // Get hero of opponent player
    this.stompClient.subscribe('/client/updategameboard/player/' + this.playerId, (response) => {
      if (response.body) {

        // Save gameboard and indicate that update has been received with setReception(true)
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);
        FacadeProperties.setReception(true);

        // Update other player's interface
        const opponentId = FacadeProperties.playerOpponent.clientId;
        this.stompClient.send('/server/sendgameboardupdate/player/' + opponentId + '/game/' + gameId);
      }
    });

    // Request gameboard update from server
    const gameId = FacadeProperties.gameId;
    this.stompClient.send('/server/updategameboard/player/' + this.playerId + '/game/' + gameId);
  }

  /**
   * Send request to server to end my turn
   */
  endMyTurn() {

    const opponentId = FacadeProperties.playerOpponent.clientId;

    // Receive confirmation from server that my turn has ended
    this.stompClient.subscribe('/client/endmyturn/player/' + this.playerId, (response) => {
      if (response.body) {

        // Save gameboard
        const gameboard = JSON.parse(response.body);
        FacadeProperties.setGameBoard(gameboard);
        FacadeProperties.setReception(true);

        // Notify player that turn has been ended
        $('#serverMessageGameBoard').html('It \'s the end of your turn.');
        $('#gameboardMessagesModal').show();

        // Notify other player that I've ended my turn
        this.stompClient.send('/server/sendendofturn/player/' + opponentId + '/game/' + gameId);
      }
    });

    // Send to server that i've ended my turn
    const gameId = FacadeProperties.gameId;
    this.stompClient.send('/server/endmyturn/player/' + this.playerId + '/opponent/' + opponentId + '/game/' + gameId);
  }

  /**
   * Disconnect web socket connexion
   */
  disconnect() {
    if (this.stompClient != null) {
      this.stompClient.close();
    }
    console.log('Disconnected');
  }

}
