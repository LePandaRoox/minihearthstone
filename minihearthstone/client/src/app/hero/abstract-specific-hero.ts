
/**
 * Abstract specific hero class
 */
export abstract class AbstractSpecificHero {
    /**
     * Name of specific hero class
     */
    name: string;
}
