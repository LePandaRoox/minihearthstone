import { Component, OnInit } from '@angular/core';
import { FacadeImpl } from '../webclientfacade/facade-impl.service';
import { Player } from '../player/player';
import { Hero } from '../hero/hero';
import { Card } from '../card/card';
import * as $ from 'jquery';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
/**
 * Gameboard component
 */
export class GameboardComponent implements OnInit {

  player: Player;
  hero: Hero;
  opponent: Player;
  opponentHero: Hero;
  handOfCards: Card[];
  cardsOnGround: Card[] = [];
  opponentCardsOnGround: Card[] = [];
  myturn: boolean;
  mytimer;
  selectedServant: Card;          // selected card on figthing ground (my card)
  selectedOpponentServant: Card;  // selected card on figthing ground (opponent's card card)
  selectedOpponentHero: Hero;     // selected opponent hero
  selectedHeroSpecial = false;    // selected hero special skill
  selectedCardInHand: Card;
  facade: FacadeImpl;

  /**
   * Constructor
   * @param newFacade Web client facade
   */
  constructor(private newFacade: FacadeImpl) {
    this.facade = newFacade;
   }

   /**
    * Lifecycle hook
    */
  ngOnInit() {

    this.player = this.facade.getPlayer();

    if (this.player == null) {
      window.location.href = '/';
    }

    this.opponent = this.facade.getPlayerOpponent();
    // this.hero = this.facade.getHero(this.player.clientId);
    this.hero = this.facade.getHero(this.player.clientId);
    this.opponentHero = this.facade.getHero(this.opponent.clientId);
    this.handOfCards = this.facade.getHandOfCards(this.player.clientId);

    // If it's not my turn continously wait for gameboard update from the server
    if (this.facade.getTurn() !== this.player.clientId) {
      this.facade.setReception(false);
      this.waitForGameBoardUpdate();
      this.myturn = false;
    } else {
      this.myturn = true;
      const that = this;

      // Else if it's my turn set a timer until 60 seconds before ending my turn
      this.setTwoMinutesTimer();
    }
  }

  /**
   * Set the timer to two minutes for each round
   */
  setTwoMinutesTimer() {
    const that = this;
    this.facade.setReception(false);

    this.mytimer = setTimeout(function () {
      that.endMyTurn();
    }, 120000); // 120000
  }

  /**
   * If still hasn't received gameboard update, wait 600ms before resending gameboard update request
   */
  waitForGameBoardUpdate() {
    const that = this;
    setTimeout(function () {
      console.log('Waiting for server response...');
      if (that.facade.getReception() === true) {

        // Update this component's interface with server response
        that.handOfCards = that.facade.getHandOfCards(that.player.clientId);
        that.cardsOnGround = that.facade.getCardsOnGround(that.player.clientId);
        that.opponentCardsOnGround = that.facade.getOpponentCardsOnGround(that.player.clientId);
        that.player = that.facade.getPlayerById(that.player.clientId);
        that.hero = that.facade.getHero(that.player.clientId);
        that.opponentHero = that.facade.getOpponentHero(that.player.clientId);
        that.opponent = that.facade.getPlayerById(that.opponent.clientId);
        that.facade.setReception(false);

        // enable end of turn button if it's my turn
        if (that.facade.getTurn() === that.player.clientId && that.myturn === false) {
          that.myturn = true;
          this.mytimer = setTimeout(function () {
            that.endMyTurn();
          }, 120000); // 120000
        }

        // Check if it's the end of the game
        if (that.facade.getWinner() !== null) {
          if (that.facade.getWinner() === that.player.clientId) {
            $('#serverMessageGameBoard').html('You\'ve won!');
          } else {
            $('#serverMessageGameBoard').html('You loose!');
          }
          $('#gameboardMessagesModal').show();
        }

        console.log('Gameboard updated');

        // If it's still not my turn to play that set reception to false and recall the function
        if (that.facade.getTurn() !== that.player.clientId) {
          that.facade.setReception(false);
          that.waitForGameBoardUpdate();
        }
      } else {
        that.waitForGameBoardUpdate();
      }
    }, 600);
  }

  /**
   * Update the component view using properties from gameboard sent from server
   * @param that reference this 'this'
   */
  updateBrowserInteface(that) {
    // Update this component's interface with server response
    that.handOfCards = this.facade.getHandOfCards(that.player.clientId);
    that.cardsOnGround = this.facade.getCardsOnGround(that.player.clientId);
    that.opponentCardsOnGround = this.facade.getOpponentCardsOnGround(that.player.clientId);
    that.player = this.facade.getPlayerById(that.player.clientId);
    that.hero = this.facade.getHero(that.player.clientId);
    that.opponentHero = this.facade.getOpponentHero(that.player.clientId);
    that.opponent = this.facade.getPlayerById(that.opponent.clientId);
    this.facade.setReception(false);

  }

  /**
   * End my turn and clear timeout for next round
   */
  endMyTurn() {
    if (this.facade.getGameBoard().turn === this.facade.getPlayer().clientId) {
      this.myturn = !this.myturn;
      console.log('End my turn');
      this.facade.setReception(false);
      this.facade.endMyTurn();
      clearTimeout(this.mytimer);

      // Wait for gameboard update from other player
      this.waitForGameBoardUpdate();
    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }
  }

  /**
   * Select hero special skill
   */
  selectHerospecial() {

    // Check if it's my turn to play
    if (this.facade.getGameBoard().turn === this.facade.getPlayer().clientId) {

      console.log('hero special');

      // Check if player has enough mana
      if (this.player.mana - 2 >= 0) {

        if (this.hero.canUseSpecialSkill) {
          if (this.hero.skillType === 'self') {

            // Send skill activation to server
            this.facade.useHeroSpecial();
            this.facade.setReception(false);
            this.resetSelection();
            this.waitForGameBoardUpdate();
  
          } else if (this.hero.skillType === 'targeted') {
            this.selectedHeroSpecial = true;
          }
        } else {
          $('#serverMessageGameBoard').html('Special skill already used. Please wait for next round');
          $('#gameboardMessagesModal').show();
        }
        
      } else {
        this.resetSelection();
          $('#serverMessageGameBoard').html('Not enough mana!');
          $('#gameboardMessagesModal').show();
      }

    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }

  }

  /**
   * Select my servant
   * @param servant my servant card
   */
  selectServant(servant: Card) {
    // Check if it's my turn to play
    if (this.facade.getGameBoard().turn === this.facade.getPlayer().clientId) {

      this.selectedServant = servant;

      if (this.selectedCardInHand == null) {
        if (this.selectedServant.canAttack === true) {
          console.log('Selected servant : ' + this.selectedServant.name);
        } else {
          this.selectedServant = null;
          $('#serverMessageGameBoard').html('This servant can\'t fight yet! Please wait for the next round');
          $('#gameboardMessagesModal').show();
        }
      } else {
        console.log('applySpellCardOnServant()');
        // If selected servant is not null then call server method to apply the card's in hand skill to servant
        this.facade.applySpellCardOnFriendlyServant(this.selectedCardInHand.uniqueId, this.selectedServant.uniqueId);
        this.facade.setReception(false);
        this.resetSelection();
        this.waitForGameBoardUpdate();
      }
    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }
  }

  /**
   * Select target servant (belonging to opponent)
   * @param target target card
   */
  selectTargetServant(target: Card) {

    // Check if it's my turn to play
    if (this.facade.getGameBoard().turn === this.facade.getPlayer().clientId) {

      this.selectedOpponentServant = target;

      console.log('Selected opponent servant : ' + this.selectedOpponentServant.name);

      // Check if it's an attack on servant -> servant
      if (this.selectedServant != null && this.selectedOpponentServant != null) {

        if (this.checkPreActionEffects(this.selectedOpponentServant)) {
          console.log('No provocation effects blocking');

          console.log('card select card');
          this.facade.applyCardOnTargetServant(this.selectedServant.uniqueId, this.selectedOpponentServant.uniqueId);
          this.facade.setReception(false);
          this.selectedServant.canAttack = false;
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('A servants\'s effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      } else if (this.selectedHeroSpecial && this.selectedOpponentServant != null) {

        if (this.checkPreActionEffects(this.selectedOpponentServant)) {

          // Else check if it's an attack on hero spell -> servant
          console.log('hero select card');
          this.facade.applyHeroSpecialOnServant(this.selectedOpponentServant.uniqueId);
          this.facade.setReception(false);
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          $('#serverMessageGameBoard').html('A servant with provocation effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      } else if (this.selectedCardInHand && this.selectedOpponentServant != null) {

        if (this.checkPreActionEffects(this.selectedOpponentServant)) {

          // Else check if it's an attack on targeted spell card -> servant
          console.log('targeted spell select card');
          this.facade.applySpellCardOnServant(this.selectedCardInHand.uniqueId, this.selectedOpponentServant.uniqueId);
          this.facade.setReception(false);
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('A servants\'s effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      }
    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }
  }

  /**
   * Select opponent hero as target
   * @param target opponent hero
   */
  selectTargetHero(target: Hero) {

    if (this.facade.getGameBoard().turn === this.facade.getPlayer().clientId) {
      this.selectedOpponentHero = target;

      console.log('Selected opponent hero : ' + this.selectedOpponentHero.name);

      if (this.selectedServant != null && this.selectedOpponentHero != null) {

        if (this.checkPreActionEffects(null)) {
          console.log('card select hero');
          this.facade.applyCardOnTargetHero(this.selectedServant.uniqueId);
          this.facade.setReception(false);
          this.selectedServant.canAttack = false;
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('A servants\'s effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      } else if (this.selectedHeroSpecial && this.selectedOpponentHero != null) {
        // Else check if it's an attack on hero spell  -> hero
        if (this.checkPreActionEffects(null)) {
          console.log('hero select hero');
          this.facade.applyHeroSpecialOnHero();
          this.facade.setReception(false);
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('A servants\'s effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      } else if (this.selectedCardInHand && this.selectedOpponentHero != null) {

        if (this.checkPreActionEffects(null)) {
          // Else check if it's an attack on targeted spell card -> hero
          console.log('targeted spell selected hero');
          this.facade.applySpellCardOnHero(this.selectedCardInHand.uniqueId);
          this.facade.setReception(false);
          this.resetSelection();
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('A servants\'s effect is blocking your attack');
          $('#gameboardMessagesModal').show();
        }
      }
    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }
  }

  /**
   * Select card in hand
   * @param card  select my card in hand
   */
  selectCardInHand(card: Card) {

    console.log('selected card: ' + card.name);

    if (this.facade.getTurn() === this.facade.getPlayer().clientId) {

      // Check if it's a targetSpell
      if (card.nature === 'targetedSpell') {
        this.selectedCardInHand = card;
        console.log('Selected target spell : ' + this.selectedCardInHand.name);
      } else {
        console.log('choose card : ' + card.name);

        // Check if player has enough mana
        if (this.player.mana - card.manacost >= 0) {
          // Update gameboard
          this.facade.setReception(false);

          // Send choice to facade
          this.facade.chooseCard(card.uniqueId);

          // Wait for gameboard update from server
          this.waitForGameBoardUpdate();
        } else {
          this.resetSelection();
          $('#serverMessageGameBoard').html('Not enough mana!');
          $('#gameboardMessagesModal').show();
        }
      }
    } else {
      this.resetSelection();
      $('#serverMessageGameBoard').html('It\'s not your turn yet!');
      $('#gameboardMessagesModal').show();
    }
  }

  /**
   * Reset all object selection in interface
   */
  resetSelection() {
    this.selectedServant = null;
    this.selectedOpponentServant = null;
    this.selectedOpponentHero = null;
    this.selectedHeroSpecial = false;
    this.selectedCardInHand = null;
  }

  /**
   * Logout
   */
  logout() {
    this.facade.logout();
  }

  /**
   * Hide gameboard message modal
   */
  hideGameboardMessagesModal() {
    $('#gameboardMessagesModal').hide();
  }

  /**
   * Show gameboard messaged modal
   */
  showGameboardMessagesModal() {
    $('#gameboardMessagesModal').show();
  }

  /**
   * Check if there are any blocking effects before performing an attack on target card
   * @param target target card
   */
  checkPreActionEffects(target: Card): boolean {
    console.log('performing : GameBoard.checkPreActionEffects()');
    return this.facade.performPreActionPlayerCardEffects(this.player.clientId, target);
  }

}


/**
 * Close modal if click detected is outside of modal
 */
window.onclick = function (event) {
  if (event.target === document.getElementById('gameboardMessagesModal')) {
    $('#gameboardMessagesModal').hide();
  }
};