export class Player {
    id: number;
    clientId: number;
    name: string;
    status: string;
    mana: number;
    stored_mana: number;
}
