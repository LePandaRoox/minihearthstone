import { Component, OnInit } from '@angular/core';
import { FacadeImpl } from '../webclientfacade/facade-impl.service';
import { Player } from '../player/player';
import * as $ from 'jquery';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
/**
 * Dashboard component
 */
export class DashboardComponent implements OnInit {

  /**
   * Player
   */
  player: Player;

  /**
   * Constructor of DashboardComponent
   * @param facade use FacadeImpl
   */
  constructor(private facade: FacadeImpl) {
  }

  /**
   * Instructions to execute on component intialization
   */
  ngOnInit() {
    this.player = this.facade.getPlayer();

    if (this.player == null) {
      window.location.href = '/';
    }

    // Modal initialisation
    $('#modalTitle').html('Matchmaking proposal');
  }

  /**
   * Get player match
   */
  getMatch() {
    this.facade.getMatch();
    $('#serverMessage').html('<p>Looking for a match ... </p>');
    $('#rejectMatchBtn').hide();
    $('#matchProposalModal').show();
  }

  /**
   * Accept the match proposed from the server
   */
  acceptMatch() {
    this.facade.acceptMatch();
    // this.hideMatchProposalModal();
    $('#serverMessage').html('<p>Waiting for ' + this.facade.getPlayerOpponent().name +
     ' to accept ... <fa name="cog" animation="spin"></fa></p>');
    $('#acceptMatchBtn').hide();
    $('#rejectMatchBtn').hide();
  }

  /**
   * Reject match proposed by the server
   */
  rejectMatch() {
    this.facade.rejectMatch();
    this.hideMatchProposalModal();
  }

  /**
   * Accept match request received from server
   */
  acceptMatchRequest() {
    this.facade.acceptMatchRequest();
    this.hideMatchProposalModal();
  }

  /**
   * Logout -> remove player from JPA Persistance and disconnect web socket
   */
  logout() {
    this.facade.logout();
  }

  /**
   * Get player info using facade
   */
  getPlayer() {
    this.facade.getPlayer();
  }

  /**
   * Get all players from JPA Persistance
   */
  getAllPlayers() {
    this.facade.getAllPlayers();
  }

  /**
   * Get all Cards from JPA
   */
  getFirstHandOfCards() {
    this.facade.getFirstHandOfCards();
  }

  // Interface interaction functions

  /**
   * Opens match proposal modal
   */
  displayMatchProposalModal() {
    $('#matcnotificationModalhProposalModal').show();
  }

  /**
   * Hides match proposal modal
   */
  hideMatchProposalModal() {
   $('#matchProposalModal').hide();
  }
}

/**
 * Close modal if click detected is outside of modal
 */
window.onclick = function(event) {
  if (event.target ===  document.getElementById('matchProposalModal')) {
    $('#matchProposalModal').hide();
  }
};



