import { Component, OnInit } from '@angular/core';

import { FacadeImpl } from '../webclientfacade/facade-impl.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
/**
 * Login component
 */
export class LoginComponent implements OnInit {

  /**
   * Boolean indicate whether username has been taken or not
   */
  showmessage = true;

  /**
   * LoginComponent constructor
   * @param facade web client facade
   */
  constructor(private facade: FacadeImpl) { }

  /**
   * Lifecycle hook
   */
  ngOnInit() {
  }

  /**
   * Login with username
   * @param playername Player name entered in login interface
   */
  login(playername) {
    this.showmessage = this.facade.login(playername);
  }

  /**
   * Request player match from web client facade
   */
  getMatch() {
    this.facade.getMatch();
  }

}
