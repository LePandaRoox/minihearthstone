
export interface Facade {

    // PROPERTIES GETTERS AND SETTERS
    

    // METHODS TO COMMUNICATE WITH SERVER

    /**
     * Send login player request to the server through ServerProxy
     * @param playername Player username
     */
    login(playername): boolean;

    /**
     * Send logout player request to the server through ServerProxy
     */
    logout();

    /**
     * Send match request to the server through ServerProxy
     */
    getMatch();

    /**
     * Send accept match proposal to the server through ServerProxy
     */
    acceptMatch();

    /**
     * Send reject match request to the server through ServerProxy
     */
    rejectMatch();

    /**
     * Send accept request to the server through ServerProxy
     */
    acceptMatchRequest();

    /**
     * Send get player request to the server through ServerProxy
     */
    getPlayerFromServer();

    /**
     * Send get all players request to the server through ServerProxy
     */
    getAllPlayers();

    /**
     * Send get all heroes request to the server through ServerProxy
     */
    getAllHeroes();

    /**
     * Send get first hand request to the server through ServerProxy
     */
    getFirstHandOfCards();

    /**
     * Send hero choice to the server through ServerProxy
     * @param heroId hero id number (stored in JPA)
     */
    chooseHero(heroId: number);

    /**
     * Send card choice to the server through ServerProxy
     * @param cardUniqueId card's unique id stored in JPA
     */
    chooseCard(cardUniqueId: string);

    /**
     * Send end my turn (in game) to the server through ServerProxy
     */
    endMyTurn();

    /**
     * Send request to apply card's skill on a target servant to the server through ServerProxy
     * @param myCardUniqueId Propriety ''uniqueId' of selected servant card on ground stored in JPA
     * @param opponentCardUniqueId Propriety 'uniqueId' of selected opponent servant card on ground stored in JPA
     */
    applyCardOnTargetServant(myCardUniqueId: string, opponentCardUniqueId: string);

    /**
     * Send request to apply card's skill on the oppoent's hero to the server through ServerProxy
     * @param myCardUniqueId Propriety 'uniqueId' of selected servant card on ground stored in JPA
     */
    applyCardOnTargetHero(myCardUniqueId: string);

    /**
     * Send request to apply my hero's special skill on a target servant to the server through ServerProxy
     * @param opponentServantUniqueId  Propriety 'uniqueId' of selected opponent servant card on ground stored in JPA
     */
    applyHeroSpecialOnServant(opponentServantUniqueId: string);

    /**
     * Send request to apply my hero's special skill on the opponen't hero to the server through ServerProxy
     */
    applyHeroSpecialOnHero();

    /**
     * Send request to apply card's spell on a target opponent servant to the server through ServerProxy
     * @param myTargetSpellCardUniqueId Propriety 'uniqueId' of selected servant card on ground stored in JPA
     * @param opponentCardUniqueId Propriety 'uniqueId' of selected opponent servant card on ground stored in JPA
     */
    applySpellCardOnServant(myTargetSpellCardUniqueId: string, opponentCardUniqueId: string);

    /**
     * Send request to apply card's spell on a one of my servants on ground to the server through ServerProxy
     * @param myTargetSpellCardUniqueId Propriety 'uniqueId' of selected servant card on ground stored in JPA
     * @param opponentCardUniqueId Propriety 'uniqueId' of selected opponent servant card on ground stored in JPA
     */
    applySpellCardOnFriendlyServant(myTargetSpellCardUniqueId: string, opponentCardUniqueId: string);

    /**
     * Send request to apply card's spell on opponent's hero to the server through ServerProxy
     * @param myTargetSpellCardUniqueId Propriety 'uniqueId' of selected servant card on ground stored in JPA
     */
    applySpellCardOnHero(myTargetSpellCardUniqueId: string);

    /**
     * Send request to apply hero's special skill on self
     */
    useHeroSpecial();

}
