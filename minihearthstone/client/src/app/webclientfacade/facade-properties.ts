
import { Player } from '../player/player';
import { Hero } from '../hero/hero';
import { Card } from '../card/card';
import { GameBoard } from '../gameboard/gameboard';

// Used before game (in dashboard and lobby)
export let player: Player;
export let playerOpponent: Player;
export let gameId: String;
export let heroes: Hero[];
export let hero: Hero;
export let opponentHero: Hero;

// Used  once game has started, mainly used in dashboard.component.ts
export let gameBoard: GameBoard = new GameBoard();
export let reception = false;

export function getGameBoard(): GameBoard {
    return gameBoard;
}

export function getHeroes(): Hero[] {
    return heroes;
}

export function getReception(): boolean {
    return reception;
}

export function setPlayer(newPlayer: Player) {
    player = newPlayer;
}

export function setOpponentHero(newHero: Hero) {
    opponentHero = newHero;
}

export function setHero(newHero: Hero) {
    hero = newHero;
}

export function setHeroes(newHeroes: Hero[]) {
    heroes = newHeroes;
}

export function setPlayerOpponent(newPlayerOpponent: Player) {
    playerOpponent = newPlayerOpponent;
}

export function setGameId(newGameId: String) {
    gameId = newGameId;
}

export function setReception(newReception: boolean) {
    reception = newReception;
}

export function setGameBoard(newGameBoard: GameBoard) {
    gameBoard = newGameBoard;
}

export function getHandOfCards(playerId: number): Card[] {
    if (playerId === this.gameBoard.p1ClientId) {
        return this.gameBoard.p1HandOfCards;
    } else if (playerId === this.gameBoard.p2ClientId) {
        return this.gameBoard.p2HandOfCards;
    }
}

export function getCardsOnGround(playerId: number): Card[] {
    if (playerId === this.gameBoard.p1ClientId) {
        return this.gameBoard.p1CardsOnGround;
    } else if (playerId === this.gameBoard.p2ClientId) {
        return this.gameBoard.p2CardsOnGround;
    }
}

export function getOpponentCardsOnGround(playerId: number) {
    if (playerId === this.gameBoard.p1ClientId) {
        return this.gameBoard.p2CardsOnGround;
    } else if (playerId === this.gameBoard.p2ClientId) {
        return this.gameBoard.p1CardsOnGround;
    }
}

export function getTurn(): number {
    return this.gameBoard.turn;
}

export function getPlayer(playerId: number): Player {
    if (this.gameBoard.players.length <= 2) {
        if (this.gameBoard.players[0].clientId === playerId) {
            return this.gameBoard.players[0];
        } else if (this.gameBoard.players[1].clientId === playerId) {
            return this.gameBoard.players[1];
        }
    }
    return null;
}

export function getPlayerOpponent(playerId: number): Player {
    if (this.gameBoard.players.length <= 2) {
        if (this.gameBoard.players[0].clientId === playerId) {
            return this.gameBoard.players[1];
        } else if (this.gameBoard.players[1].clientId === playerId) {
            return this.gameBoard.players[0];
        }
    }
    return null;
}

export function getHero(playerId: number): Hero {
    if (this.gameBoard.players.length <= 2) {
        if (this.gameBoard.players[0].clientId === playerId) {
            return this.gameBoard.heroes[0];
        } else if (this.gameBoard.players[1].clientId === playerId) {
            return this.gameBoard.heroes[1];
        }
    }
    return null;
}

export function getOpponentHero(playerId: number): Hero {
    if (this.gameBoard.players.length <= 2) {
        if (this.gameBoard.players[0].clientId === playerId) {
            return this.gameBoard.heroes[1];
        } else if (this.gameBoard.players[1].clientId === playerId) {
            return this.gameBoard.heroes[0];
        }
    }
    return null;
}

export function performPreActionPlayerCardEffects(playerId: number, targetCard: Card): boolean {

    console.log('proptertis.performPreActionPlayerCardEffects()');

    let effectResponse = true;

    // Get opponent player's cards on ground
    let cardsOnGround: Card[] = [];
    if (playerId === this.gameBoard.p1ClientId) {
        cardsOnGround = this.gameBoard.p2CardsOnGround;
    } else if (playerId === this.gameBoard.p2ClientId) {
        cardsOnGround = this.gameBoard.p1CardsOnGround;
    }

    // Stop when found a card, whose's effect answers false
    let index = 0;
    while (index < cardsOnGround.length) {

        const card: Card = new Card();
        card.clone(cardsOnGround[index]);

        console.log('current Card: ' + JSON.stringify(card));
        console.log('target Card: ' + JSON.stringify(targetCard));

        // If card's pre-action effect return false, then set effectResponse to false;
        if (!card.applyPreActionEffect(playerId, targetCard)) {
            effectResponse = false;
            break;
        }
        index++;
    }
    return effectResponse;
}

export function getWinner(): number {
    return this.gameBoard.winner;
}
