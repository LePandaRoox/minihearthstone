import { GameBoard } from '../gameboard/gameboard';
import { AbstractEffect } from './effect/abstract-effect';
import { AbstractSpecificCard } from './abstract-specific-card';
import { ProvocationEffect } from './effect/provocation-effect';
import { ChargeEffect } from './effect/charge-effect';
import { LifeStealEffect } from './effect/lifesteal-effect';

export class Card {

    // PROPRITIES
    id: number;
    uniqueId: string;
    name: string;
    manacost: number;
    damage: number;
    lifepoints: number;
    defense: number;
    type: string;
    description: string;
    nature: string;
    canAttack: boolean;
    gbP1HandOfCardsMapping: GameBoard;
    gbP2HandOfCardsMapping: GameBoard;
    gbP1CardsOnGroundMapping: GameBoard;
    gbP2CardsOnGroundMapping: GameBoard;
    effect: AbstractEffect;
    specificCard: AbstractSpecificCard;

    // CONSTRUCTOR
    public Card() {

    }

    // METHODS

    /**
     * Create exact copy of card passed in parameter
     * @param card card to be cloned
     */
    public clone(card: Card) {
        this.id = card.id;
        this.uniqueId = card.uniqueId;
        this.name = card.name;
        this.manacost = card.manacost;
        this.damage = card.damage;
        this.lifepoints = card.lifepoints;
        this.defense = card.defense;
        this.type = card.type;
        this.description = card.description;
        this.nature = card.nature;
        this.canAttack = card.canAttack;
        this.gbP1HandOfCardsMapping = card.gbP1HandOfCardsMapping;
        this.gbP2HandOfCardsMapping = card.gbP2HandOfCardsMapping;
        this.gbP1CardsOnGroundMapping = card.gbP1CardsOnGroundMapping;
        this.gbP2CardsOnGroundMapping = card.gbP2CardsOnGroundMapping;
        this.effect = card.effect;

        // Instanciate effect
        if (card.effect != null) {
            if (this.effect.name === 'ProvocationEffect') {
                card.effect = new ProvocationEffect();
            } else if (this.effect.name === 'ChargeEffect') {
                card.effect = new ChargeEffect();
            } else if (this.effect.name === 'LifeStealEffect') {
                card.effect = new LifeStealEffect();
            }
        }
        this.specificCard = card.specificCard;
    }

    /**
     * Apply pre action effect
     * @param playerId client id of player who owns this card
     * @param targetCard target card
     */
    public applyPreActionEffect(playerId: number, targetCard: Card) {
        console.log('Card.targetCard.applyPreActionEffect() -> ' + JSON.stringify(targetCard));

        let effect: AbstractEffect = null;
        let effectName = '';
        let targetCardUniqueId = '';

        // Get target effect name
        if (targetCard != null) {
            if (targetCard.effect != null) {
                effectName = targetCard.effect.name;
            }
            if (targetCard.uniqueId != null) {
                targetCardUniqueId = targetCard.uniqueId;
            }
        }

        // Initiliaze this effect
        if (this.effect != null) {
            if (this.effect.name === 'ProvocationEffect') {
                effect = new ProvocationEffect();
            } else if (this.effect.name === 'ChargeEffect') {
                effect = new ChargeEffect();
            } else if (this.effect.name === 'LifeStealEffect') {
                effect = new LifeStealEffect();
            }
        }

        if (effect !== null) {
            return effect.applyPreActionEffect(this.uniqueId, playerId, targetCardUniqueId, effectName);
        } else {
            console.log('Effect is equal to null');
            return true;
        }
    }

}
