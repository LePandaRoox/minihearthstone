## Minihearthsone server

## Build

In */server* folder run command :
Windows: `gradle build`
Linux: `./gradlew build`

## Run

In */server* folder run command :

Windows: `gradle bootRun`
Linux: `./gradlew bootRun`

## Javadoc

[Documentation de code Javadoc](https://gitlab.com/LePandaRoox/minihearthstone/tree/frontbackdev/minihearthstone/server/javadoc/html).