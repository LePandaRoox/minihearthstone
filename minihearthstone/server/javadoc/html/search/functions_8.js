var searchData=
[
  ['imagemirroir',['ImageMirroir',['../classminihearthstone_1_1entities_1_1cards_1_1_image_mirroir.html#a11c57cfae89d0486aa2ca5ffe58fd088',1,'minihearthstone::entities::cards::ImageMirroir']]],
  ['incrementmana',['incrementMana',['../classminihearthstone_1_1entities_1_1player_1_1_player.html#a60abc4aba8fd25a02c1f14c3b8964538',1,'minihearthstone::entities::player::Player']]],
  ['initgame',['initGame',['../classminihearthstone_1_1controller_1_1_client_proxy.html#afd76cdbdf63fb3d759650a88b16c5a3b',1,'minihearthstone.controller.ClientProxy.initGame()'],['../classminihearthstone_1_1facade_1_1_facade.html#a5b5991fed781ba86643f6a302a9142df',1,'minihearthstone.facade.Facade.initGame()'],['../interfaceminihearthstone_1_1facade_1_1_i_facade.html#a7114ad3c29ff09c7cef3d947bb29470a',1,'minihearthstone.facade.IFacade.initGame()']]],
  ['isendofgame',['isEndOfGame',['../classminihearthstone_1_1entities_1_1gameboard_1_1_game_board.html#ad13a48e6935bb169f64f5b874558f032',1,'minihearthstone.entities.gameboard.GameBoard.isEndOfGame()'],['../interfaceminihearthstone_1_1entities_1_1gameboard_1_1_i_game_board.html#a2322ae33cf8f17934be986d079fff7b9',1,'minihearthstone.entities.gameboard.IGameBoard.isEndOfGame()']]]
];
