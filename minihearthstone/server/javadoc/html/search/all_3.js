var searchData=
[
  ['damage',['damage',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#ae8036d19b75764d90418220e0df521df',1,'minihearthstone::entities::cards::Card']]],
  ['deck',['Deck',['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html',1,'minihearthstone.entities.deck.Deck'],['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#ae781d6d8e53b3d10cd619743b67deee6',1,'minihearthstone.entities.cards.Card.deck()'],['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#a6af0fbf78e7ecce6607750958bdb3fe5',1,'minihearthstone.entities.deck.Deck.Deck()'],['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#aae16cc0f7792ca4a77f8d942b906baed',1,'minihearthstone.entities.deck.Deck.Deck(Hero hero)'],['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#aa227e3c0b695eed8f45e42431d62aa60',1,'minihearthstone.entities.deck.Deck.Deck(Hero hero, List&lt; Card &gt; commonCards, List&lt; Card &gt; heroSpecificCards)']]],
  ['deck_2ejava',['Deck.java',['../_deck_8java.html',1,'']]],
  ['deckrepository',['DeckRepository',['../interfaceminihearthstone_1_1entities_1_1deck_1_1_deck_repository.html',1,'minihearthstone::entities::deck']]],
  ['deckrepository_2ejava',['DeckRepository.java',['../_deck_repository_8java.html',1,'']]],
  ['deductmana',['deductMana',['../classminihearthstone_1_1entities_1_1player_1_1_player.html#a21a51cb347b35210a9f182f6b032e77f',1,'minihearthstone::entities::player::Player']]],
  ['defense',['defense',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#a5d21d0322279f6b306b272954f8bd610',1,'minihearthstone.entities.cards.Card.defense()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_hero.html#ab2783ba4ffa16d9c2eb7adb73e4463ca',1,'minihearthstone.entities.heroes.Hero.defense()']]],
  ['deletebyclientid',['deleteByClientId',['../interfaceminihearthstone_1_1entities_1_1player_1_1_player_repository.html#a48ec73a3f4babcd42cc496a44b04af7d',1,'minihearthstone::entities::player::PlayerRepository']]],
  ['deletebyp1clientid',['deleteByP1ClientId',['../interfaceminihearthstone_1_1entities_1_1gameboard_1_1_game_board_repository.html#aae4effb8bf43a4c36c7fb52d3568ca50',1,'minihearthstone::entities::gameboard::GameBoardRepository']]],
  ['deletebyp2clientid',['deleteByP2ClientId',['../interfaceminihearthstone_1_1entities_1_1gameboard_1_1_game_board_repository.html#ae3ab9599297671d15bea432a34f803b3',1,'minihearthstone::entities::gameboard::GameBoardRepository']]],
  ['demo',['demo',['../classminihearthstone_1_1_minihearthstone_application.html#a95e16058b11984c96997ed0435065344',1,'minihearthstone::MinihearthstoneApplication']]],
  ['description',['description',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#a83fac90b64956992afbe2a0a0da0b673',1,'minihearthstone.entities.cards.Card.description()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_hero.html#a5df9b96bcd6f4538e9baa17bc3a12d81',1,'minihearthstone.entities.heroes.Hero.description()']]]
];
