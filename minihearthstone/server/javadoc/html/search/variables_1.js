var searchData=
[
  ['damage',['damage',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#ae8036d19b75764d90418220e0df521df',1,'minihearthstone::entities::cards::Card']]],
  ['deck',['deck',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#ae781d6d8e53b3d10cd619743b67deee6',1,'minihearthstone::entities::cards::Card']]],
  ['defense',['defense',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#a5d21d0322279f6b306b272954f8bd610',1,'minihearthstone.entities.cards.Card.defense()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_hero.html#ab2783ba4ffa16d9c2eb7adb73e4463ca',1,'minihearthstone.entities.heroes.Hero.defense()']]],
  ['description',['description',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#a83fac90b64956992afbe2a0a0da0b673',1,'minihearthstone.entities.cards.Card.description()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_hero.html#a5df9b96bcd6f4538e9baa17bc3a12d81',1,'minihearthstone.entities.heroes.Hero.description()']]]
];
