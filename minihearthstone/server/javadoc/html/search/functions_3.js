var searchData=
[
  ['deck',['Deck',['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#a6af0fbf78e7ecce6607750958bdb3fe5',1,'minihearthstone.entities.deck.Deck.Deck()'],['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#aae16cc0f7792ca4a77f8d942b906baed',1,'minihearthstone.entities.deck.Deck.Deck(Hero hero)'],['../classminihearthstone_1_1entities_1_1deck_1_1_deck.html#aa227e3c0b695eed8f45e42431d62aa60',1,'minihearthstone.entities.deck.Deck.Deck(Hero hero, List&lt; Card &gt; commonCards, List&lt; Card &gt; heroSpecificCards)']]],
  ['deductmana',['deductMana',['../classminihearthstone_1_1entities_1_1player_1_1_player.html#a21a51cb347b35210a9f182f6b032e77f',1,'minihearthstone::entities::player::Player']]],
  ['deletebyclientid',['deleteByClientId',['../interfaceminihearthstone_1_1entities_1_1player_1_1_player_repository.html#a48ec73a3f4babcd42cc496a44b04af7d',1,'minihearthstone::entities::player::PlayerRepository']]],
  ['deletebyp1clientid',['deleteByP1ClientId',['../interfaceminihearthstone_1_1entities_1_1gameboard_1_1_game_board_repository.html#aae4effb8bf43a4c36c7fb52d3568ca50',1,'minihearthstone::entities::gameboard::GameBoardRepository']]],
  ['deletebyp2clientid',['deleteByP2ClientId',['../interfaceminihearthstone_1_1entities_1_1gameboard_1_1_game_board_repository.html#ae3ab9599297671d15bea432a34f803b3',1,'minihearthstone::entities::gameboard::GameBoardRepository']]],
  ['demo',['demo',['../classminihearthstone_1_1_minihearthstone_application.html#a95e16058b11984c96997ed0435065344',1,'minihearthstone::MinihearthstoneApplication']]]
];
