var searchData=
[
  ['lifepoints',['lifepoints',['../classminihearthstone_1_1entities_1_1cards_1_1_card.html#a09729750aacea86d00c986c8a5925649',1,'minihearthstone.entities.cards.Card.lifepoints()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_hero.html#a27fbc8cf020a71c518dec4b7a9a0e2d4',1,'minihearthstone.entities.heroes.Hero.lifepoints()']]],
  ['log',['log',['../classminihearthstone_1_1entities_1_1cards_1_1_abstract_specific_card.html#a3ca816aec88285c3d645760f2de262d6',1,'minihearthstone.entities.cards.AbstractSpecificCard.log()'],['../classminihearthstone_1_1entities_1_1cards_1_1effects_1_1_abstract_effect.html#ac03d159cb0dfefb1883aadc6f126fdc5',1,'minihearthstone.entities.cards.effects.AbstractEffect.log()'],['../classminihearthstone_1_1entities_1_1heroes_1_1_abstract_specific_hero.html#a5bee2c7b7ec14c6befbf1dfe58405757',1,'minihearthstone.entities.heroes.AbstractSpecificHero.log()']]]
];
