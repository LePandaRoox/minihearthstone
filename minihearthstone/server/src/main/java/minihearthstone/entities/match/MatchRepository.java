package minihearthstone.entities.match;

import org.springframework.data.repository.CrudRepository;

/**
 * Match repository interface used to query JPA for the Match entity
 */
public interface MatchRepository extends CrudRepository<Match, Long> {

  /**
   * Find match from player1 client id
   * @param p1ClientId client generated player 1 id
   * @return match
   */
  Match findByP1ClientId(String p1ClientId);

  /**
   * Find match from player2 client id
   * @param p2ClientId client generated player 2 id
   * @return match
   */
  Match findByP2ClientId(String p2ClientId);
}
