package minihearthstone.entities.match;

/**
 * Match interface
 */
public interface IMatch {

    // GETTERS
    /**
     * Get id
     * @return id
     */
    Long getId();

    /**
     * Get p1 answer
     * @return p1 answer (accept or reject match)
     */
    String getP1Answer();

    /**
     * Get p2 answer
     * @return p2 answer (accept or reject match)
     */
    String getP2Answer();

    /**
     * Get player 1 client id
     * @return player 1 client id
     */
    String getPlayer1();

    /**
     * Get player 2 client id
     * @return player 2 client id
     */
    String getPlayer2();

    // SETTERS
    /**
     * Set p1 answer accept or reject match)
     * @param p1Answer answer
     */
    void setP1Answer(String p1Answer);

    /**
     * Set p2 answer accept or reject match)
     * @param p2Answer answer
     */
    void setP2Answer(String p2Answer);

    /**
     * Set player 1 client id
     * @param p1ClientId player 1 client id
     */
    void setPlayer1(String p1ClientId);

    /**
     * Set player 2 client id
     * @param p2ClientId player 2 client id
     */
    void setPlayer2(String p2ClientId);

    /**
     * Set player's answer
     * @param playerClientId player answering
     * @param answer player's answer
     */
    void setAnswer(String playerClientId, String answer);

}