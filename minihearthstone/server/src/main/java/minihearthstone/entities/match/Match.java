package minihearthstone.entities.match;

// Spring and java imports
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.transaction.Transactional;

@Entity
@Transactional
public class Match implements IMatch {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id; // used for gameId in client side
    private String p1Answer;
    private String p2Answer;
    private String p1ClientId;
    private String p2ClientId;

    protected Match() {
    };

    public Match(String p1Answer, String p2Answer, String p1ClientId, String p2ClientId) {
        this.p1Answer = p1Answer;
        this.p2Answer = p2Answer;
        this.p1ClientId = p1ClientId;
        this.p2ClientId = p2ClientId;
    }

    // GETTERS
    public Long getId() {
        return this.id;
    }

    public String getP1Answer() {
        return this.p1Answer;
    }

    public String getP2Answer() {
        return this.p2Answer;
    }

    public String getPlayer1() {
        return this.p1ClientId;
    }

    public String getPlayer2() {
        return this.p2ClientId;
    }

    // SETTERS

    public void setP1Answer(String p1Answer) {
        this.p1Answer = p1Answer;
    }

    public void setP2Answer(String p2Answer) {
        this.p2Answer = p2Answer;
    }

    public void setPlayer1(String p1ClientId) {
        this.p1ClientId = p1ClientId;
    }

    public void setPlayer2(String p2ClientId) {
        this.p2ClientId = p2ClientId;
    }

    // METHODS
    public void setAnswer(String playerClientId, String answer) {
        
        if (playerClientId.equals(p1ClientId)) {
            this.p1Answer = answer;
        }
        else if (playerClientId.equals(p2ClientId)){
            this.p2Answer = answer;
        }
    }

}
