package minihearthstone.entities.deck;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import org.springframework.beans.factory.annotation.Autowired;
import minihearthstone.entities.cards.Card;
import minihearthstone.entities.cards.CardRepository;
import minihearthstone.entities.heroes.Hero;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Entity
public class Deck implements IDeck {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;
    private Long heroId;

    @OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="deck")
    List<Card> deck = new ArrayList<Card>();

    @Autowired
    @Transient
    private CardRepository cardRepository;

    @Transient
    private static final Logger log = LoggerFactory.getLogger(Deck.class);

    protected Deck() { }

    public Deck(Hero hero) {
        this.name = hero.getName() + "'s card deck";
        this.heroId = hero.getId();
    }

    public Deck(Hero hero, List<Card> commonCards, List<Card> heroSpecificCards) {
        this.name = hero.getName() + "'s card deck";
        this.heroId = hero.getId();
        
        // Add all elements of common and heroSpecific to deck
        for (Card card : commonCards) {
            this.addToDeck(card);
        }
        
        for (Card card : heroSpecificCards) {
            this.addToDeck(card);
        }
    }

    @Override
  public String toString() {
    return String.format(
            "Deck[id=%d, name='%s', heroId=%d, cards{'%s'}]",
            this.id, this.name, this.heroId, this.getCardNames());
  }

    // Getters
    @Column(insertable = false, updatable = false)
    public Long getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public List<Card> getDeck() {
        return this.deck;
    }
    public Long getHeroId() {
        return this.heroId;
    }

    // Setters
	public void setId(Long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public void setDeck(List<Card> decklist) {
        this.deck = decklist; // TODO : handle bidirection association
    }
    public void setHeroId(Long heroId) {
        this.heroId = heroId;
    }

    // Methods 
    public String getCardNames() {
        String names = "";
        for(Card card: this.deck) {
            names += card.getName() + " ";
        }
        return names;
    }
    
    public void addToDeck(Card card) {
        Card cardCopy = card.uniqueCopy();
        cardCopy.setDeck(this);
        this.deck.add(cardCopy);
    }
    
    public Card pickCard(List<Card> deck) {
        int random = (int) (Math.random() * (deck.size()-1));
        log.info("Random pick index: " + random);
        return deck.get(random);
    }
    
    public List<Card> randomHand(int numberOfCards) {
        List<Card> handOfCards = new ArrayList<>();
        List<Card> cardsundertwo = new ArrayList<>();
    
        // Vérifier au cas où numberOfCards passé en paramètre serai trop grand
        if (numberOfCards > this.deck.size()) {
            numberOfCards = this.deck.size();
        }
    
        for(int i = 0; i < this.deck.size(); i++) {
            if (this.deck.get(i).getManacost() <= 2 && !cardsundertwo.contains(this.deck.get(i)))
                cardsundertwo.add(this.deck.get(i));
        }
    
        for (int i = 0; i < 2; i++) {
            handOfCards.add(pickCard(cardsundertwo));
        }
    
        for (int i = 0; i < (numberOfCards - 2); i++) {
            handOfCards.add(pickCard(this.deck).uniqueCopy());
        }
        //return handOfCards;
        return handOfCards;
    }
    
}
