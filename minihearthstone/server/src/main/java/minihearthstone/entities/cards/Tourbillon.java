package minihearthstone.entities.cards;

import java.util.List;

import javax.persistence.Entity;

import minihearthstone.entities.gameboard.GameBoard;

@Entity
public class Tourbillon extends AbstractSpecificCard {

    public Tourbillon() {
        setName("Tourbillon");
    }

    @Override
    public void specialSkill(Long clientId, GameBoard gameBoard) {
        List<Card> opponentServants = gameBoard.getOpponentCardsOnGround(clientId);
        List<Card> servants = gameBoard.getPlayerCardsOnGround(clientId);
        for (Card servant : opponentServants) {
            servant.setLifepoints(servant.getLifepoints()-1);
        }
        for (Card servant : servants) {
            servant.setLifepoints(servant.getLifepoints()-1);
        }
	}

    @Override
    public void specialSkillOnDeath(Long playerId, GameBoard gameBoard) {

    }

    @Override
    public void specialSkillOnServant(String cardId, Long playerId, GameBoard gameBoard) {

    }

    @Override
    public void specialSkillOnHero(Long playerId, GameBoard gameBoard) {

    }
    
}