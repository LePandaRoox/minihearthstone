package minihearthstone.entities.cards;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import minihearthstone.entities.cards.effects.ProvocationEffect;
import minihearthstone.entities.gameboard.GameBoard;

@Entity
public class ImageMirroir extends AbstractSpecificCard {

    public ImageMirroir() {
        setName("ImageMirroir");
    }

    @Override
    public void specialSkill(Long playerId, GameBoard gameBoard) {
        List<Card> newServants = new ArrayList<>();
        newServants.add(new BasicCard("Image mirroir", 1, 1, 1, "common", "servant", new ProvocationEffect(), null));
        newServants.add(new BasicCard("Image mirroir", 1, 1, 1, "common", "servant", new ProvocationEffect(), null));
        gameBoard.addToPlayerCardsOnGround(playerId, newServants);
	}

    @Override
    public void specialSkillOnDeath(Long playerId, GameBoard gameBoard) {

    }

    @Override
    public void specialSkillOnServant(String cardId, Long playerId, GameBoard gameBoard) {

    }

    @Override
    public void specialSkillOnHero(Long playerId, GameBoard gameBoard) {

    }
    
}