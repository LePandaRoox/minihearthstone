package minihearthstone;

import minihearthstone.entities.cards.*;
import minihearthstone.entities.cards.effects.ChargeEffect;
import minihearthstone.entities.cards.effects.LifeStealEffect;
import minihearthstone.entities.cards.effects.ProvocationEffect;
import minihearthstone.entities.deck.Deck;
import minihearthstone.entities.deck.DeckRepository;
import minihearthstone.entities.heroes.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.SpringVersion;

import java.util.ArrayList;
import java.util.List;


@SpringBootApplication
public class MinihearthstoneApplication {

	private static final Logger log = LoggerFactory.getLogger(MinihearthstoneApplication.class);

	public static void main(final String[] args) {
		log.info("version: " + SpringVersion.getVersion());
		SpringApplication.run(MinihearthstoneApplication.class, args);
		
	}

	@Bean
	public CommandLineRunner demo(
		final CardRepository cardRepository, 
		final HeroRepository heroRepository, 
		final DeckRepository deckRepository) 
	{
		return (args) -> {
			// Define card lists
			final List<Card> commonCards = new ArrayList<>();
			final List<Card> paladinSpecificCards = new ArrayList<>();
			final List<Card> warriorSpecificCards = new ArrayList<>();
			final List<Card> mageSpecificCards = new ArrayList<>();
			String common = "common";
			String paladinstr = "paladin";
			String warriorstr = "warrior";
			String magestr = "mage";
			String servantstr = "servant";
			String spellstr = "spell";


			// Store the common cards
			commonCards.add(new BasicCard("Sanglier brocheroc", 1, 1, 1, common, servantstr, null, null));
			commonCards.add(new BasicCard("Soldat du comté-de-l'or", 1, 1, 2, common,servantstr,new ProvocationEffect(), null));
			commonCards.add(new BasicCard("Chevaucheur de loup", 3, 3, 1, common,servantstr, new ChargeEffect(), null));
			commonCards.add(new BasicCard("Chef de Raid", 3, 2, 2, common,servantstr, null, new ChefDeRaid()));
			commonCards.add(new BasicCard("Yéti noroit", 4, 4, 5, common,servantstr, null, null));

			// Paladin specific cardsapplyPreActionEffect
			paladinSpecificCards.add(new BasicCard("Champion frisselame", 4, 3, 2, paladinstr,servantstr, new LifeStealEffect(), null));
			paladinSpecificCards.add(new BasicCard("Bénédiction de puissance", 1, 3, 0, paladinstr,"targetedSpell", null, new BenedictionDePuissance()));
			paladinSpecificCards.add(new BasicCard("Consécration", 4, 2, 0, paladinstr,spellstr, null, new Consecration()));

			// Warrior specific cards
			warriorSpecificCards.add(new BasicCard("Tourbillon", 1, 1, 0, "warriorstr",spellstr, null, new Tourbillon()));
			warriorSpecificCards.add(new BasicCard("Avocat commis d'office", 2, 0, 7, "warriorstr",servantstr, new ProvocationEffect(), null));
			warriorSpecificCards.add(new BasicCard("Maîtrise du blocage", 3, 0, 0, "warriorstr",spellstr,null, new MaitriseDuBlocage()));

			// Mage specific cards
			mageSpecificCards.add(new BasicCard("Image miroir", 1, 0, 2, magestr,spellstr, new ProvocationEffect(), new ImageMirroir()));
			mageSpecificCards.add(new BasicCard("Explosion des arcanes", 2, 1, 0, magestr,spellstr, null, new ExplosionDesArcanes()));
			mageSpecificCards.add(new BasicCard("Métamorphose", 4, 0, 0, magestr,"targetedSpell", null, new Metamorphose()));

			// Store the heroes
			final Hero paladin = new BasicHero("Echalwe", paladinstr, "self", new Paladin(), "Renfort, invoquant un serviteur 'recrue de la Main d'argent' 1/1.");
			final Hero warrior =  new BasicHero("Hrothgar", warriorstr, "self", new Warrior(), "Armure, lui conférant 2 points d'armure");
			final Hero mage = new BasicHero("Athelas", magestr, "targeted",new Mage(), "Boule de feu, infligeant un point de dégat à l'adversaire (serviteur ou héros).");

			heroRepository.save(paladin);
			heroRepository.save(warrior);
			heroRepository.save(mage);

			// Initialize deck of each hero

			final Deck paladinDeck = new Deck(paladin, commonCards, paladinSpecificCards);
			final Deck warriorDeck = new Deck(warrior, commonCards, warriorSpecificCards);
			final Deck mageDeck = new Deck(mage, commonCards, mageSpecificCards);

			
			// Store the decks in JPA
			deckRepository.save(paladinDeck);
			deckRepository.save(warriorDeck);
			deckRepository.save(mageDeck);

			//Affichage test
			
			log.info("CardRepository :");
			log.info("-------------------------------------------------");
			for (final Card card : cardRepository.findAll()) {
				log.info(card.toString());
			}

			log.info("HeroRepository :");
			log.info("-------------------------------------------------");
			for (final Hero hero : heroRepository.findAll()) {
				log.info(hero.toString());
			}

			log.info("commonCards :");
			log.info("-------------------------------------------------");
			for (final Card card : commonCards) {
				log.info(card.toString());
			}

			log.info("paladinSpecificCards :");
			log.info("-------------------------------------------------");
			for (final Card card : paladinSpecificCards) {
				log.info(card.toString());
			}

			log.info("DeckRepository :");
			log.info("-------------------------------------------------");
			for (final Deck deck : deckRepository.findAll()) {
				log.info(deck.toString());
			}

			log.info("-------------------------------------------------");
			/*Deck paladinDeckJPA = deckRepository.findByHeroId(paladin.getId());
			log.info(paladinDeckJPA.toString());*/

			
			// log.info(paladinDeck.randomHand(3).toString());

		};
	}
}
