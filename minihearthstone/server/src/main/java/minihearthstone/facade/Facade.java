package minihearthstone.facade;

import minihearthstone.controller.ClientProxy;
import minihearthstone.entities.cards.Card;
import minihearthstone.entities.cards.CardRepository;
import minihearthstone.entities.deck.Deck;
import minihearthstone.entities.deck.DeckRepository;
import minihearthstone.entities.gameboard.GameBoard;
import minihearthstone.entities.gameboard.GameBoardRepository;
import minihearthstone.entities.heroes.BasicHero;
import minihearthstone.entities.heroes.Hero;
import minihearthstone.entities.heroes.HeroRepository;
import minihearthstone.entities.match.Match;
import minihearthstone.entities.match.MatchRepository;
import minihearthstone.entities.player.Player;
import minihearthstone.entities.player.PlayerRepository;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

@Component("Facade")
public class Facade implements  IFacade {

    private static final Logger log = LoggerFactory.getLogger(ClientProxy.class);

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private HeroRepository heroRepository;

    @Autowired
    private GameBoardRepository gameBoardRepository;

    @Autowired
    private DeckRepository deckRepository;

    @Autowired
    private CardRepository cardRepository;

    public Facade() {

    }

    @Override
    public Player login(String playerId, String playername) {

        // Create instance of player
        Player player = new Player(playername, Long.parseLong(playerId));

        // Check if player already exists in database
        if (playerRepository.findByClientId(Long.parseLong(playerId)) != null) {
            player = playerRepository.findByClientId(Long.parseLong(playerId));
            player.setClientId(Long.parseLong("-1"));
        } else {
            if (playerRepository.findByName(playername).isEmpty()) {
                // Save player in JPA Persistence
                playerRepository.save(player);
            } else {
                player.setClientId(Long.parseLong("-1"));
            }
        }
        return player;
    }

    @Override
    public String logout(String playerId) {

        // Find all components associated to player and delete them
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        Player opponent = null;

        if (player != null) {
            log.info("Logging out player : " + player.getName());
            playerRepository.delete(player);
        }

        GameBoard gameboard = gameBoardRepository.findByP1ClientId(Long.parseLong(playerId));

        // If the gameboard is null, it means that playerId refers to the second player
        if (gameboard == null) {
            gameboard = gameBoardRepository.findByP2ClientId(Long.parseLong(playerId));
        }
        if (gameboard != null) {
            opponent = new Player(gameboard.getOpponentPlayer(Long.parseLong(playerId)).getName(), gameboard.getOpponentPlayer(Long.parseLong(playerId)).getClientId());
            gameBoardRepository.delete(gameboard);
        }

        // Save the deleted player from the gameboard
        if (opponent != null) {
            playerRepository.save(opponent);
        }

        // If the match is null, it means that playerId refers to the second player
        Match match = matchRepository.findByP1ClientId(playerId);
        if (match == null) {
            match = matchRepository.findByP2ClientId(playerId);
        }
        if (match != null) {
            matchRepository.delete(match);
        }

        // Show in log all remaining players, matches and games
        log.info("Remaining players : " + playerRepository.findAll().toString());
        log.info("Remaining matches : " + matchRepository.findAll().toString());
        log.info("Remaining games : " + gameBoardRepository.findAll().toString());

        return "Logged out succesffuly";
    }

    @Override
    public String logout(String playerId, String opponentId) {
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        Player opponent = playerRepository.findByClientId(Long.parseLong(opponentId));

        opponent.setStatus("available");
        playerRepository.save(opponent);
        return player.getName() + " has logged out. Redirection you to dashboard ...";
    }

    @Override
    public int pregetMatch(String playerId) {
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        player.setStatus("looking for match");
        playerRepository.save(player);

        List<Player> players = convertToList(playerRepository.findByStatus("looking for match"));
        return players.size();
    }

    @Override
    public Player getMatch(String playerId) {

        // Get all players from database
        List<Player> players = convertToList(playerRepository.findByStatus("looking for match"));
        log.info("number of players looking for match : " + players.size());

        if (players.size() > 0) {
            // Pick and return random player from player list
            int size = players.size();
            int random = (int) (Math.random() * (size));

            log.info("size: " + size);
            log.info("random: " + random);

            // Get random player match
            Player randomPlayer = players.get(random);
            while (randomPlayer.getClientId() == Long.parseLong(playerId)) {
                random = (int) (Math.random() * (size));
                randomPlayer = players.get(random);
            }

            // Set player status to match found to avoid getting matched with another player
            Player player = playerRepository.findByClientId(Long.parseLong(playerId));
            Player opponent = players.get(random);

            player.setStatus("match found");
            opponent.setStatus("match found");
            System.out.println("ok");
            playerRepository.save(player);
            playerRepository.save(opponent);
            // Create match and save in database
            Match newMatch = new Match("", "", playerId, String.valueOf(randomPlayer.getClientId()));
            matchRepository.save(newMatch);

            return players.get(random);
        } else {
            return null;
        }
    }

    @Override
    public Match acceptMatch(String playerId, String opponentId) {

        // Get corresponding match
        Match match = matchRepository.findByP1ClientId(playerId);
        if (match == null) {
            match = matchRepository.findByP2ClientId(playerId);
        }
        // Set player answer
        match.setAnswer(playerId, "accept");
        matchRepository.save(match);

        // Change player status to in-lobby
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        player.setStatus("in-lobby");
        playerRepository.save(player);

        // Return match object
        return match;
    }

    @Override
    public Match rejectMatch(String playerId) {

        // Get corresponding match
        Match match = matchRepository.findByP1ClientId(playerId);
        if (match == null) {
            match = matchRepository.findByP2ClientId(playerId);
        }

        // Change player status to available
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        player.setStatus("available");
        playerRepository.save(player);

        // Set player answer
        match.setAnswer(playerId, "reject");

        // Delete the match from the database
        matchRepository.delete(match);

        // Return match object
        return match;
    }

    @Override
    public Match acceptMatchRequest(String playerId, String opponentId) {

        // Get corresponding match
        Match match = matchRepository.findByP1ClientId(playerId);
        if (match == null) {
            match = matchRepository.findByP2ClientId(playerId);
        }

        // Change player status to in-lobby
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        player.setStatus("in-lobby");
        playerRepository.save(player);

        return match;
    }

    @Override
    public String rejectMatchRequest(String playerId, String opponentId) {

        // Get opponent player
        Player opponent = playerRepository.findByClientId(Long.parseLong(playerId));

        // Get corresponding match
        Match match = matchRepository.findByP1ClientId(playerId);
        if (match == null) {
            match = matchRepository.findByP2ClientId(playerId);
        }

        // Change player status to in-lobby
        Player player = playerRepository.findByClientId(Long.parseLong(opponentId));
        player.setStatus("looking for match");
        playerRepository.save(player);

        // Delete the match from the database
        matchRepository.delete(match);

        return opponent.getName() + " has rejected your challenge.";
    }

    @Override
    public Player getPlayer(String playerId) {
        return playerRepository.findByClientId(Long.parseLong(playerId));
    }

    @Override
    public boolean removePlayer(String playerId) {

        // Get player, then delete
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));
        playerRepository.delete(player);

        // TODO : make a true verification of deletion
        return true;
    }

    @Override
    public List<Player> getAllPlayers() {
        return convertToList(playerRepository.findAll());
    }


    @Override
    public List<Hero> getAllHeroes() {
        return convertToList(heroRepository.findAll());
    }

    @Override
    public GameBoard chooseHero(String playerId, String heroId, String gameId) {

        // Get player from JPA
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));

        // Set hero id to player
        player.setHeroId(Long.parseLong(heroId));
        playerRepository.save(player);

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get the hero object from JPA
        Hero hero = heroRepository.findById(Long.parseLong(heroId)).get();

        // If game board doesn't exist, create it and set player id
        if (gameBoard == null) {
            gameBoard = new GameBoard(Long.parseLong(gameId));

            // Save game board in JPA
            gameBoardRepository.save(gameBoard);

            gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));
            log.info("New gameboard :" + gameBoard.getPlayers().toString());
        }
        // If the game exists set player as p2
        else {
            log.info("Already created gameboard");
        }
        gameBoard.addPlayer(player);
        gameBoard.addHero(hero.uniqueCopy());

        // Save game board in JPA
        gameBoardRepository.save(gameBoard);

        // Get updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        log.info("Updated gameboard : " + updatedGameBoard.getPlayers().toString());
        log.info("Updated gameboard : " + updatedGameBoard.getHeroes().toString());
        log.info("Updated gameboard to string : " + updatedGameBoard.toString());

        // Return hero
        return updatedGameBoard;
    }

    @Override
    public GameBoard getGameBoardByGameId(String gameId) {
        // Get match from JPA
        return gameBoardRepository.findByGameId(Long.parseLong(gameId));
    }

    @Override
    public Hero getHeroByPlayerId(String playerId) {

        // Get player from JPA
        Player player = playerRepository.findByClientId(Long.parseLong(playerId));

        if (player != null) {
            // Get the player's hero
            return heroRepository.findById(player.getHeroId()).get();
        } else {
            return new BasicHero();
        }
    }

    @Override
    public GameBoard initGame(String playerId, String gameId) {

        log.info("Received initgame from player: " + playerId);

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // If starting player not yet set
        if (gameBoard.getTurn() == null) {
            // Randomly set starting player
            int random = new Random().nextInt((1 - 0) + 1) + 0;
            log.info("Random  :" + random);
            if (random == 0) {
                gameBoard.setTurn(gameBoard.getP1ClientId());
                log.info("The lucky winner is " + gameBoard.getP1ClientId());
            } else if (random == 1) {
                gameBoard.setTurn(gameBoard.getP2ClientId());
                log.info("The lucky winner is " + gameBoard.getP2ClientId());
            }
            gameBoardRepository.save(gameBoard);
        }
        return gameBoard;
    }

    @Override
    public GameBoard chooseCard(String playerId, String gameId, String cardUniqueId) {

        log.info("Received chooseCard() from player : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));
        log.info("Before update : " + gameBoard.toString());

        Card card = gameBoard.findCardInHandOfCardsByUniqueId(Long.parseLong(playerId), cardUniqueId);

        if (card != null) {

            // Check if player has enough mana
            if (gameBoard.getPlayer(Long.parseLong(playerId)).checkEnoughMana(card.getManacost())) {

                // Deduct mana points according to card cost
                gameBoard.getPlayer(Long.parseLong(playerId)).deductMana(card.getManacost());

                if (card != null) {

                    // Apply card on choice effect
                    card.applyOnChoiceEffect(card, Long.parseLong(playerId), gameBoard);
                    log.info("Card after applyOnChoiceEffect() -> " + card.toString());

                    // If card is a servant add it to the ground
                    if (card.getNature().equals("servant")) {
                        gameBoard.addToPlayerCardsOnGround(Long.parseLong(playerId), card);

                        // Apply all different possible effects to this newly added card
                        gameBoard.applyAllDifferentEffectsOnTarget(Long.parseLong(playerId), card.getUniqueId());
                    }

                    // Apply card special skill
                    card.specialSkill(Long.parseLong(playerId), gameBoard);

                    // Remove card from hand of cards
                    gameBoard.removeFromPlayerHandOfCards(Long.parseLong(playerId), card);

                }
            }
        }
        gameBoardRepository.save(gameBoard);

        // Send updated gameboard back to client
        GameBoard gameBoardUpdated = gameBoardRepository.findByGameId(Long.parseLong(gameId));
        log.info("After update : " + gameBoardUpdated.toString());

        return gameBoardUpdated;
    }

    @Override
    public GameBoard cardAttackServant(String playerId, String opponentId, String gameId, String servantUniqueIds) {

        log.info("Received cardAttackServant() from player : " + playerId);
                log.info("-------------------------------------------------");

                // Get gameboard from JPA
                GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));
                JSONParser parser = new JSONParser();

                String servantUniqueId = "";
                String opponentServantUniqueId = "";
                playerId = playerId.replaceAll("\\s+", "");
                opponentId = opponentId.replaceAll("\\s+", "");

                try {
                    Object obj = parser.parse(servantUniqueIds);
                    JSONArray array = (JSONArray) obj;

                    servantUniqueId = (String) array.get(0);
                    opponentServantUniqueId = (String) array.get(1);

                } catch (ParseException pe) {
                    log.info("position: " + pe.getPosition());
                    log.info("Pe: " + pe);
                }

                if (!servantUniqueId.equals("") && !opponentServantUniqueId.equals("")) {

                    // Get the player's selected card on ground
                    Card servantCard = gameBoard.findCardOnGroundByUniqueId(Long.parseLong(playerId), servantUniqueId);
                    log.info("Checking ability to attack");

                    // If card is allowed to attack this round, and if card effects allow it
                    if (servantCard.getCanAttack() && gameBoard.performPreActionOpponentCardEffects(Long.parseLong(playerId), opponentServantUniqueId)) {
                        // Attack opponent card
                        servantCard.normalAttackOnServant(opponentServantUniqueId, Long.parseLong(opponentId), gameBoard);
                    }
                    else {
                        log.info("There are card effects blocking the attack");
                    }
                }
                log.info("After attack : ");
                gameBoard.printPlayerCardsOnGroundInfo(Long.parseLong(opponentId));

                // Save the gameboard
                gameBoardRepository.save(gameBoard);

                // Return updated gameboard from JPA
                GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

                return updatedGameBoard;
    }

    @Override
    public GameBoard spellCardAttackServant(String playerId, String opponentId, String gameId, String servantUniqueIds) {

        log.info("Received spellCardAttackServant() from player : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));
        JSONParser parser = new JSONParser();

        String spellCardUniqueId = "";
        String opponentServantUniqueId = "";
        playerId = playerId.replaceAll("\\s+", "");
        opponentId = opponentId.replaceAll("\\s+", "");

        try {
            Object obj = parser.parse(servantUniqueIds);
            JSONArray array = (JSONArray) obj;

            spellCardUniqueId = (String) array.get(0);
            opponentServantUniqueId = (String) array.get(1);

        } catch (ParseException pe) {
            log.info("position: " + pe.getPosition());
            log.info("Pe: " + pe);
        }

        if (!spellCardUniqueId.equals("") && !opponentServantUniqueId.equals("")) {

            // Get the player's selected card on ground
            Card spellCard = gameBoard.findCardInHandOfCardsByUniqueId(Long.parseLong(playerId), spellCardUniqueId);

            // If other card effects allow it
            if (gameBoard.performPreActionOpponentCardEffects(Long.parseLong(playerId), opponentServantUniqueId)) {
                // Attack opponent card
                spellCard.specialSkillOnServant(opponentServantUniqueId, Long.parseLong(opponentId), gameBoard);

                // Remove card from hand of cards
                gameBoard.removeFromPlayerHandOfCards(Long.parseLong(playerId), spellCard);
            }
            else {
                log.info("There are card effects blocking the attack");
            }
        }
        log.info("After attack : ");
        gameBoard.printPlayerCardsOnGroundInfo(Long.parseLong(opponentId));

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard spellCardOnFriendlyServant( String playerId, String opponentId, String gameId, String servantUniqueIds) {

        log.info("Received spellCardOnFriendlyServant() from player : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));
        JSONParser parser = new JSONParser();

        String spellCardUniqueId = "";
        String servantUniqueId = "";
        playerId = playerId.replaceAll("\\s+", "");
        opponentId = opponentId.replaceAll("\\s+", "");

        try {
            Object obj = parser.parse(servantUniqueIds);
            JSONArray array = (JSONArray) obj;

            spellCardUniqueId = (String) array.get(0);
            servantUniqueId = (String) array.get(1);

        } catch (ParseException pe) {
            log.info("position: " + pe.getPosition());
            log.info("Pe: " + pe);
        }

        if (!spellCardUniqueId.equals("") && !servantUniqueId.equals("")) {

            // Get the player's selected card on ground
            Card spellCard = gameBoard.findCardInHandOfCardsByUniqueId(Long.parseLong(playerId), spellCardUniqueId);

            // Apply special skill on servant card
            spellCard.specialSkillOnServant(servantUniqueId, Long.parseLong(playerId), gameBoard);

            // Remove card from hand of cards
            gameBoard.removeFromPlayerHandOfCards(Long.parseLong(playerId), spellCard);
        }
        log.info("After special skill application : ");
        gameBoard.printPlayerCardsOnGroundInfo(Long.parseLong(playerId));

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard cardAttackHero(String playerId, String opponentId, String gameId, String servantUniqueId) {

        log.info("Received cardAttackHero() from player : " + playerId);
        log.info("-------------------------------------------------");

        playerId = playerId.replaceAll("\\s+", "");
        opponentId = opponentId.replaceAll("\\s+", "");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get servant card
        Card servantCard = gameBoard.findCardOnGroundByUniqueId(Long.parseLong(playerId), servantUniqueId);

        // If other card effects allow it
        if (gameBoard.performPreActionOpponentCardEffects(Long.parseLong(playerId), "")) {
            servantCard.normalAttackOnHero(Long.parseLong(opponentId), gameBoard);
        }
        else {
            log.info("There are card effects blocking the attack");
        }

        log.info("servantUniqueId: " + servantCard.toString());

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard spellCardAttackHero(String playerId,  String opponentId, String gameId, String spellCardUniqueId) {

        log.info("Received spellcardattackhero() from player : " + playerId);
        log.info("-------------------------------------------------");

        playerId = playerId.replaceAll("\\s+", "");
        opponentId = opponentId.replaceAll("\\s+", "");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get spell card
        Card spellCard = gameBoard.findCardOnGroundByUniqueId(Long.parseLong(playerId), spellCardUniqueId);

        // If other card effects allow it
        if (gameBoard.performPreActionOpponentCardEffects(Long.parseLong(playerId), "")) {
            spellCard.normalAttackOnHero(Long.parseLong(opponentId), gameBoard);

            // Remove spellcard from hand on cards
            gameBoard.removeFromPlayerHandOfCards(Long.parseLong(playerId), spellCard);
        }
        else {
            log.info("There are card effects blocking the attack");
        }

        log.info("spellCardUniqueId: " + spellCard.toString());

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard heroAttackServant(String playerId, String opponentId, String gameId, String opponentServantUniqueId) {

        log.info("Received heroAttackServant() from player : " + playerId);
        log.info("-------------------------------------------------");

        log.info("cardUniqueId : " + opponentServantUniqueId);

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get hero from gameboard
        Hero hero = gameBoard.getHero(Long.parseLong(playerId));

        // Apply hero special skill on opponent servant
        hero.specialSkillOnServant(playerId, opponentServantUniqueId, gameBoard);

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard heroAttackHero(String playerId, String opponentId, String gameId) {

        log.info("Received heroAttackHero() from player : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get hero from gameboard
        Hero hero = gameBoard.getHero(Long.parseLong(playerId));

        // Apply hero special skill on servant
        hero.specialSkillOnHero(playerId, gameBoard);

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard useHeroSpecial(String playerId, String opponentId, String gameId) {

        log.info("Received useHeroSpecial() from player : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Get hero from gameboard
        Hero hero = gameBoard.getHero(Long.parseLong(playerId));
        log.info("Hero : " + hero.toString());

        // Apply hero special skill on servant
        hero.specialSkill(playerId, gameBoard);

        // log.info("After special skill : " + hero.toString());

        // Save the gameboard
        gameBoardRepository.save(gameBoard);

        // Return updated gameboard from JPA
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public GameBoard getFirstHandOfCards(String playerId, String heroId, String gameId) {

        log.info("Received getfirsthand() from player : " + playerId);
        log.info("-------------------------------------------------");

        List<Card> handOfCards = new ArrayList<>();

        // Get deck of corresponding hero from JPA
        Deck deck = deckRepository.findByHeroId(Long.parseLong(heroId));

        // Get the gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        if (deck == null)
            log.info("Deck is null");

        // If this player is the first player to start, pick 3 cards randomly
        if (gameBoard != null && gameBoard.getTurn() != null) {
            if (gameBoard.getTurn().longValue() == Long.parseLong(playerId)) {
                handOfCards = deck.randomHand(3);
                log.info("You are the lucky winner! You have been given 3 cards");
            }
            // Else pick 4 cards
            else {
                handOfCards = deck.randomHand(3);
                log.info("The other player is starting first. You've been given 3 cards.");
            }
        } else {
            log.info("game board is null");
        }

        log.info("Gameboard before hand of cards added : " + gameBoard.toString() + System.lineSeparator());

        // Put first hand of cards in gameboard
        if (gameBoard.getP1ClientId().longValue() == Long.parseLong(playerId)) {
            log.info("P1 client id : " + gameBoard.getP1ClientId().longValue());
            gameBoard.addToP1HandOfCards(handOfCards);
        } else if (gameBoard.getP2ClientId().longValue() == Long.parseLong(playerId)) {
            log.info("P2 client id : " + gameBoard.getP2ClientId().longValue());
            gameBoard.addToP2HandOfCards(handOfCards);
        }

        gameBoardRepository.save(gameBoard);

        log.info("Hand of cards saved in gameboard : " + handOfCards.toString());

        GameBoard gameBoardUpdated = gameBoardRepository.findByGameId(Long.parseLong(gameId));
        log.info("Gameboard after hand of cards added : " + gameBoardUpdated.toString() + System.lineSeparator());

        return gameBoardUpdated;
    }

    @Override
    public GameBoard updateGameBoard(String playerId, String gameId) {

        /*
        log.info("Received updateGameboard() from player : " + playerId);
        log.info("-------------------------------------------------");
        */
        // Get gameboard (already update by other player)
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Remove any dead servants from gameboard
        gameBoard.removeDeadServantsFromGround(Long.parseLong(playerId));

        // Check if we have a winner
        gameBoard.isEndOfGame();

        gameBoardRepository.save(gameBoard);

        // Get gameboard (already update by other player)
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // log.info("Updated gameboard : " + updatedGameBoard.toString());

        return updatedGameBoard;
    }

    @Override
    public GameBoard endMyTurn(String playerId, String opponentId, String gameId) {

        log.info("Receive end of turn from : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard from JPA
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Change turn
        gameBoard.setTurn(Long.parseLong(opponentId));

        // Make cards on ground able to attack for next round
        gameBoard.cardsOnGroundCanAttack(Long.parseLong(playerId));

        // Enable hero special skill for next round
        gameBoard.getHero(Long.parseLong(playerId)).setCanUseSpecialSkill(true);

        // Save gameboard
        gameBoardRepository.save(gameBoard);

        // Get updated gameboard
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        log.info("Gameboard updated after end of turn : " + gameBoard.toString());

        return updatedGameBoard;
    }

    @Override
    public GameBoard sendEndOfTurn(String playerId, String gameId) {

        log.info("Notifiy end of turn to : " + playerId);
        log.info("-------------------------------------------------");

        // Get gameboard (already updated by other player)
        GameBoard gameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        // Increment player mana
        gameBoard.getPlayer(Long.parseLong(playerId)).incrementMana();

        // TEMPORARY : limit on number of card's in hand 7 to avoid socket overload
        int size = gameBoard.getPlayerHandOfCards(Long.parseLong(playerId)).size();
        if (gameBoard.getPlayerHandOfCards(Long.parseLong(playerId)).size() < 7) {
            log.info("There's less than 7 cards in hand ");
            // Add random picked card to player's hand of cards
            Deck deck = deckRepository.findByHeroId(gameBoard.getPlayer(Long.parseLong(playerId)).getHeroId());
            gameBoard.pickCardFromDeckToPlayerHand(Long.parseLong(playerId), deck);
        }
        gameBoardRepository.save(gameBoard);

        // Get updated gameboard
        GameBoard updatedGameBoard = gameBoardRepository.findByGameId(Long.parseLong(gameId));

        return updatedGameBoard;
    }

    @Override
    public int size(Iterable<?> iterable) {
        if (iterable instanceof Collection)
            return ((Collection<?>) iterable).size();

        // Else iterate
        int i = 0;
        for (Object obj : iterable)
            i++;
        return i;
    }

    @Override
    public List convertToList(Iterable iterable) {
        List target = new ArrayList();
        iterable.forEach(target::add);
        return target;
    }

}
