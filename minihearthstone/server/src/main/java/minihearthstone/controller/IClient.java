package minihearthstone.controller;
import minihearthstone.entities.gameboard.GameBoard;
import minihearthstone.entities.heroes.Hero;
import minihearthstone.entities.match.Match;
import minihearthstone.entities.player.Player;

import java.util.List;

/**
 * Client interface : contains all functions linked to exchanges between the client and the server
 */
interface IClient {

    /**
     * Receive login request from client
     * @param playerId client generated playerId client generated playerId
     * @param playername player username
     * @return newly saved player object in JPA
     */
    Player login(String playerId, String playername);

    /**
     * Receive logout request from client
     * @param playerId client generated playerId
     * @return notification message
     */
    String logout(String playerId);

    /**
     * Receive request to notify other opponent that player has logged out
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return notification message
     */
    String logout(String playerId, String opponentId);

    /**
     * Receive request from client to start looking for match
     * @param playerId client generated playerId
     * @return number of players looking for match
     */
    int pregetMatch(String playerId);

    /**
     * Receive match request from client
     * @param playerId client generated playerId
     * @return player
     */
    Player getMatch(String playerId);

    /**
     * Receive match acceptation from client
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return match
     */
    Match acceptMatch(String playerId, String opponentId);

    /**
     * Receive match rejection from client
     * @param playerId client generated playerId
     * @return match
     */
    Match rejectMatch(String playerId);

    /**
     * Receive request from client to send notification that match has been accepted to the other player
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return player
     */
    Player sendMatchRequest(String playerId, String opponentId);

    /**
     * Receive match request acceptation from client
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return match
     */
    Match acceptMatchRequest(String playerId, String opponentId);

    /**
     * Receive match request rejection from client
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return notification message
     */
    String rejectMatchRequest(String playerId, String opponentId);

    /**
     * Receive request from client get player object
     * @param playerId client generated playerId
     * @return player
     */
    Player getPlayer(String playerId);

    /**
     * Receive request from client to remove player
     * @param playerId client generated playerId
     * @return true if player successfully removed, false otherwise
     */
    boolean removePlayer(String playerId);

    /**
     * Receive request from client to get a list of all players
     * @param playerId client generated playerId
     * @return list of players
     */
    List<Player> getAllPlayers(String playerId);

    /**
     * Receive request from client to get a of all heroes
     * @param playerId client generated playerId
     * @return list of heroes
     */
    List<Hero> getAllHeroes(String playerId);

    /**
     * Receive player's choice of card from client
     * @param playerId client generated playerId
     * @param heroId player's chosen hero id
     * @param gameId game id
     * @return gameboard
     */
    GameBoard chooseHero(String playerId, String heroId, String gameId);

    /**
     * Receive request from client to send notification to other player that hero has been chosen
     * @param playerId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard hasChosenHero(String playerId, String gameId);

    /**
     * Receive request to send hero choice to other player's client
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @return hero
     */
    Hero sendMyHero(String playerId, String opponentId);

    /**
     * Receive request from client to initialize game
     * @param playerId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard initGame(String playerId, String gameId);

    /**
     * Receive card choice from client
     * @param playerId client generated playerId
     * @param gameId game id
     * @param cardUniqueId
     * @return gameboard
     */
    GameBoard chooseCard(String playerId, String gameId, String cardUniqueId);

    /**
     * Receive request from client to use servant card to attack opponent's servant
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param servantUniqueIds
     * @return gameboard
     */
    GameBoard cardAttackServant(String playerId, String opponentId, String gameId, String servantUniqueIds);

    /**
     * Receive request from client to use spell card to attack opponent's servant
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param servantUniqueIds
     * @return gameboard
     */
    GameBoard spellCardAttackServant(String playerId, String opponentId, String gameId, String servantUniqueIds);

    /**
     * Receive request from client to use spell card's special skill on friendly servant on ground
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param servantUniqueIds
     * @return gameboard
     */
    GameBoard spellCardOnFriendlyServant( String playerId, String opponentId, String gameId, String servantUniqueIds);

    /**
     * Receive request from client to use card to attack opponent hero
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param servantUniqueId
     * @return gameboard
     */
    GameBoard cardAttackHero(String playerId, String opponentId, String gameId, String servantUniqueId);

    /**
     * Receive request from client to use spell card to attack opponent's hero
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param spellCardUniqueId
     * @return gameboard
     */
    GameBoard spellCardAttackHero(String playerId,  String opponentId, String gameId, String spellCardUniqueId);

    /**
     * Receive request from client to use hero's special skill to attack opponent's servant
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @param opponentServantUniqueId
     * @return gameboard
     */
    GameBoard heroAttackServant(String playerId, String opponentId, String gameId, String opponentServantUniqueId);

    /**
     * Receive request from client to use hero's special skill to attack opponent's hero
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard heroAttackHero(String playerId, String opponentId, String gameId);

    /**
     * Receive request from client to use hero's special skill on self
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard useHeroSpecial(String playerId, String opponentId, String gameId);

    /**
     * Receive request from client to send first hand of cards
     * @param playerId client generated playerId
     * @param heroId player's chosen hero id
     * @param gameId game id
     * @return gameboard
     */
    GameBoard getFirstHandOfCards(String playerId, String heroId, String gameId);

    /**
     * Receive request from client to update gameboard
     * @param playerId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard updateGameBoard(String playerId, String gameId);

    /**
     * Receive request from client to send gameboard to opponent player
     * @param playerId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard sendGameboardUpdate(String playerId, String gameId);

    /**
     * Receive request from client to end player's turn
     * @param playerId client generated playerId
     * @param opponentId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard endMyTurn(String playerId, String opponentId, String gameId);

    /**
     * Receive request from client to send end of turn to opponent player
     * @param playerId client generated playerId
     * @param gameId game id
     * @return gameboard
     */
    GameBoard sendEndOfTurn(String playerId, String gameId);

}