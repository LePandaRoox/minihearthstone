package minihearthstone.controller;

// Spring and Java imports
import minihearthstone.entities.gameboard.GameBoard;
import minihearthstone.entities.heroes.Hero;
import minihearthstone.entities.match.Match;
import minihearthstone.entities.player.Player;
import minihearthstone.facade.IFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.List;

// Minishearthstone imports

@Controller
public class ClientProxy implements IClient {

    private static final Logger log = LoggerFactory.getLogger(ClientProxy.class);

    protected ClientProxy() {};

    @Autowired
    private transient IFacade facade;

    @Override
    @MessageMapping("/login/player/{playerId}/playername/{playername}")
    @SendTo("/client/login/player/{playerId}")
    public Player login(@DestinationVariable final String playerId, @DestinationVariable final String playername) {
        return facade.login(playerId, playername);
    }

    @Override
    @MessageMapping("/logout/player/{playerId}")
    @SendTo("/client/logout/player/{playerId}")
    public String logout(@DestinationVariable final String playerId) {
        return facade.logout(playerId);
    }

    @Override
    @MessageMapping("/notifylogout/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/receivenotifylogout/player/{opponentId}")
    public String logout(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return facade.logout(playerId, opponentId);
    }

    @Override
    @MessageMapping("/lookingformatch/player/{playerId}")
    @SendTo("/client/lookingformatch/player/{playerId}")
    public int pregetMatch(@DestinationVariable final String playerId) {
        return facade.pregetMatch(playerId);
    }

    @Override
    @MessageMapping("/requestmatch/player/{playerId}")
    @SendTo("/client/requestmatch/player/{playerId}")
    public Player getMatch(@DestinationVariable final String playerId) {
        return facade.getMatch(playerId);
    }


    @Override
    @MessageMapping("/acceptmatch/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/acceptmatch/player/{playerId}")
    public Match acceptMatch(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return this.facade.acceptMatch(playerId, opponentId);
    }

    @Override
    @MessageMapping("/rejectmatch/player/{playerId}")
    @SendTo("/client/rejectmatch/player/{playerId}")
    public Match rejectMatch(@DestinationVariable final String playerId) {
        return facade.rejectMatch(playerId);
    }

    @Override
    @MessageMapping("/sendmatchrequest/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/sendmatchrequest/player/{opponentId}")
    public Player sendMatchRequest(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return facade.getPlayer(playerId);
    }

    @Override
    @MessageMapping("/acceptmatchrequest/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/acceptmatchrequest/player/{opponentId}")
    public Match acceptMatchRequest(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return facade.acceptMatchRequest(playerId, opponentId);
    }

    @Override
    @MessageMapping("/rejectmatchrequest/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/rejectmatchrequest/player/{opponentId}")
    public String rejectMatchRequest(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return facade.rejectMatchRequest(playerId, opponentId);
    }

    @Override
    @MessageMapping("/playerinfo/player/{playerId}")
    @SendTo("/client/playerinfo/player/{playerId}")
    public Player getPlayer(@DestinationVariable final String playerId) {
        return facade.getPlayer(playerId);
    }

    @Override
    @MessageMapping("/removeplayer/player/{playerId}")
    @SendTo("/client/removeplayer/player/{playerId}")
    public boolean removePlayer(@DestinationVariable final String playerId) {
        return facade.removePlayer(playerId);
    }

    @Override
    @MessageMapping("/getallplayers/player/{playerId}")
    @SendTo("/client/getallplayers/player/{playerId}")
    public List<Player> getAllPlayers(@DestinationVariable final String playerId) {
        return facade.getAllPlayers();
    }

    @Override
    @MessageMapping("/getallheroes/player/{playerId}")
    @SendTo("/client/getallheroes/player/{playerId}")
    public List<Hero> getAllHeroes(@DestinationVariable final String playerId) {
        return facade.getAllHeroes();
    }

    @Override
    @MessageMapping("/choosehero/player/{playerId}/hero/{heroId}/game/{gameId}")
    @SendTo("/client/choosehero/player/{playerId}")
    public GameBoard chooseHero(@DestinationVariable final String playerId, @DestinationVariable final String heroId,
                                @DestinationVariable final String gameId) {
        return facade.chooseHero(playerId, heroId, gameId);
    }

    @Override
    @MessageMapping("/haschosenhero/player/{playerId}/game/{gameId}")
    @SendTo("/client/haschosenhero/player/{playerId}")
    public GameBoard hasChosenHero(@DestinationVariable final String playerId, @DestinationVariable final String gameId) {
        return facade.getGameBoardByGameId(gameId);
    }

    @Override
    @MessageMapping("/sendmyhero/player/{playerId}/opponent/{opponentId}")
    @SendTo("/client/receivehero/player/{opponentId}")
    public Hero sendMyHero(@DestinationVariable final String playerId, @DestinationVariable final String opponentId) {
        return facade.getHeroByPlayerId(playerId);
    }

    @Override
    @MessageMapping("/initgame/player/{playerId}/game/{gameId}")
    @SendTo("/client/initgame/player/{playerId}")
    public GameBoard initGame(@DestinationVariable final String playerId, @DestinationVariable final String gameId) {
        return facade.initGame(playerId, gameId);
    }

    @Override
    @MessageMapping("/choosecard/player/{playerId}/game/{gameId}")
    @SendTo("/client/choosecard/player/{playerId}")
    public GameBoard chooseCard(@DestinationVariable final String playerId, @DestinationVariable final String gameId,
                                final String cardUniqueId) {
        return facade.chooseCard(playerId, gameId, cardUniqueId);
    }

    @Override
    @MessageMapping("/cardattackservant/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/cardattackservant/player/{playerId}")
    public GameBoard cardAttackServant(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                       @DestinationVariable final String gameId, final String servantUniqueIds) {
        return facade.cardAttackServant(playerId, opponentId, gameId, servantUniqueIds);
    }

    @Override
    @MessageMapping("/spellcardattackservant/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/spellcardattackservant/player/{playerId}")
    public GameBoard spellCardAttackServant(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                            @DestinationVariable final String gameId, final String servantUniqueIds) {
        return facade.spellCardAttackServant(playerId, opponentId, gameId, servantUniqueIds);
    }

    @Override
    @MessageMapping("/spellcardonfriendlyservant/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/spellcardonfriendlyservant/player/{playerId}")
    public GameBoard spellCardOnFriendlyServant(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                                @DestinationVariable final String gameId, final String servantUniqueIds) {
        return facade.spellCardOnFriendlyServant(playerId, opponentId, gameId, servantUniqueIds);
    }

    @Override
    @MessageMapping("/cardattackhero/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/cardattackhero/player/{playerId}")
    public GameBoard cardAttackHero(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                    @DestinationVariable final String gameId, final String servantUniqueId) {
        return facade.cardAttackHero(playerId, opponentId, gameId, servantUniqueId);
    }

    @Override
    @MessageMapping("/spellcardattackhero/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/spellcardattackhero/player/{playerId}")
    public GameBoard spellCardAttackHero(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                         @DestinationVariable final String gameId, final String spellCardUniqueId) {

        return facade.spellCardAttackHero(playerId, opponentId, gameId, spellCardUniqueId);
    }

    @Override
    @MessageMapping("/heroattackservant/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/heroattackservant/player/{playerId}")
    public GameBoard heroAttackServant(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                       @DestinationVariable final String gameId, final String opponentServantUniqueId) {
        return facade.heroAttackServant(playerId, opponentId, gameId, opponentServantUniqueId);
    }

    @Override
    @MessageMapping("/heroattackhero/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/heroattackhero/player/{playerId}")
    public GameBoard heroAttackHero(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                    @DestinationVariable final String gameId) {
        return facade.heroAttackHero(playerId, opponentId, gameId);
    }

    @Override
    @MessageMapping("/useherospecial/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/useherospecial/player/{playerId}")
    public GameBoard useHeroSpecial(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                                    @DestinationVariable final String gameId) {
        return facade.useHeroSpecial(playerId, opponentId, gameId);
    }

    @Override
    @MessageMapping("/getfirsthand/player/{playerId}/hero/{heroId}/game/{gameId}")
    @SendTo("/client/getfirsthand/player/{playerId}")
    public GameBoard getFirstHandOfCards(@DestinationVariable final String playerId, @DestinationVariable final String heroId,
                                         @DestinationVariable final String gameId) {
        return facade.getFirstHandOfCards(playerId, heroId, gameId);
    }

    @Override
    @MessageMapping("/updategameboard/player/{playerId}/game/{gameId}")
    @SendTo("/client/updategameboard/player/{playerId}")
    public GameBoard updateGameBoard(@DestinationVariable final String playerId, @DestinationVariable final String gameId) {
        return facade.updateGameBoard(playerId, gameId);
    }

    @Override
    @MessageMapping("/sendgameboardupdate/player/{playerId}/game/{gameId}")
    @SendTo("/client/receivegameboardupdate/player/{playerId}")
    public GameBoard sendGameboardUpdate(@DestinationVariable final String playerId, @DestinationVariable final String gameId) {
        return facade.getGameBoardByGameId(gameId);
    }

    @Override
    @MessageMapping("/endmyturn/player/{playerId}/opponent/{opponentId}/game/{gameId}")
    @SendTo("/client/endmyturn/player/{playerId}")
    public GameBoard endMyTurn(@DestinationVariable final String playerId, @DestinationVariable final String opponentId,
                               @DestinationVariable final String gameId) {
        return facade.endMyTurn(playerId, opponentId, gameId);
    }

    @Override
    @MessageMapping("/sendendofturn/player/{playerId}/game/{gameId}")
    @SendTo("/client/receiveendofturn/player/{playerId}")
    public GameBoard sendEndOfTurn(@DestinationVariable final String playerId, @DestinationVariable final String gameId) {
        return facade.sendEndOfTurn(playerId, gameId);
    }

}