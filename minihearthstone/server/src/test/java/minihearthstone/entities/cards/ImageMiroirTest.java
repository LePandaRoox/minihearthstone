package minihearthstone.entities.cards;

import javax.persistence.Entity;
import minihearthstone.entities.gameboard.GameBoard;
import minihearthstone.entities.cards.effects.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;



import static minihearthstone.AssertAnnotations.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ImageMiroirTest{

    @InjectMocks
    private ImageMirroir img;

    @Mock
    private GameBoard gameBoard;

    private final Long CL1ID = new Long("0");
    private final static String NAME1 = "name";
    private final static String NAME2 = "n2";
    private final static int MANA_COST = 1;
    private final static int DMG = 1;
    private final static int DMG2 = 2;
    private final static int HP = 1;
    private final static int HP2 = 2;
    private final static String TYPE = "type";
    private final static String NATURE = "servant";
    private final BasicEffect BASIC_EFFECT = new BasicEffect();
    private final AbstractSpecificCard SPEC_CARD = new ChefDeRaid();
    
    @Mock
    private final Card c1 = new BasicCard(NAME1,MANA_COST,DMG,HP,TYPE,NATURE,BASIC_EFFECT,SPEC_CARD);
    @Mock
    private final Card c2 = new BasicCard(NAME2,MANA_COST+1,DMG2,HP2,TYPE,NATURE,BASIC_EFFECT,SPEC_CARD);
    
    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    /*
     TODO : Cry a lot because it's not working anymore
     * Test for the method {@link ImageMirroir#specialSkill(Long, GameBoard)}
     
    @Test
    public void testSpecialSkill(){
        List<Card> lC = new ArrayList<Card>();
        lC.add(c1);
        lC.add(c2);

        img.specialSkill(CL1ID,gameBoard);
 
        verify(gameBoard).addToPlayerCardsOnGround(CL1ID,any());
    }
    */

    @Test
    public void checkPlayerAnnotations(){
        assertClassAnnotation(ImageMirroir.class, Entity.class);
    }

}