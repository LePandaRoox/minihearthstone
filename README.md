# Project initialisé avec GRADLE/ANGULAR/SPRING

* [Voir code serveur Angular](https://gitlab.com/LePandaRoox/minihearthstone/tree/frontbackdev/minihearthstone/client)
* [Voir code serveur Spring](https://gitlab.com/LePandaRoox/minihearthstone/tree/frontbackdev/minihearthstone/server)

## Bugs 
* On ne peut pas envoyer de messages trop rapidement car les web socket n'a pas le temps de répondre et ce qui fait que le client reste toujours en attente (dans ce cas il faut redémarrer le serveur)
* Le nombre de cartes en main et sur le plateau a été limité à 4-5 car si on trop de cartes le web socket se bloque et envoie une exception 

## Version utilisé 

Pour voir la version de node.js : node --version  
Pour voir la version de npm : npm -v  

Node.js -> version 11.1.0  
Npm -> 4.2.0

## Mac, Linux
Pour Npm :

`sudo npm cache clean -f`  
`sudo npm -install -g n`  
`sudo n stable`  
`sudo n latest`  


Après avoir installé npm/node.js, il faut installer Angular : `npm install -g @angular/cli`  

## Windows
Installer Angular : `npm install -g @angular/cli` 

Pour Npm :  
`npm cache clean -f`  


n ne supportant pas Windows il faut télécharger nvs (https://github.com/jasongin/nvs)  


## Lancement du Projet

Pour build le projet se mettre dans minihearthstone (où se trouve le pom.xml), puis executer la commande `mvn spring-boot:run` pour Spring ou `mvn compile`.  

##### Installer le module :

`npm install --save-dev @angular-devkit/build-angular`

##### Installer les librairies *stompjs* et *socksjs* : 

`npm install stompjs --save`

`npm install sockjs-client --save`

##### Installer les librairies *jquery* et *bootstrap* : 

`npm install jquery --save`

`npm install bootstrap`

##### Lancement du service Angular
Dans le dossier **minihearthstone/client**  
Pour lancer le service Angular `ng serve` ou `ng serve --open`(ouvrir automatiquement l'onglet dans le navigateur) .  
Pour lancer un deuxième client : `ng serve --port 4210`  
###### Erreur `Cannot find module '@angular/compiler-cli'`
Utiliser la commande `npm install` dans le dossier "minihearthstone/client"    

##### Lancement du serveur Spring
Dans le dossier : **minihearthstone/server**

###### Linux/Mac
`./gradlew bootRun` 

<<<<<<< HEAD
###### Windows
`gradle bootRun`
=======
## Lancement des Tests 

#### Lancement de l'interface de la couverture de code :

Dans le dossier **"minihearthstone/client"** effectuer la commande `ng test --no-watch --code-coverage`.

Ensuite aller dans le dossier **"/coverage"** et lancer le fichier *'index.html'*.

__Remarque :__ Ce dossier doit se créer automatiquement lors de l'exécution de la commande `ng serve`.

#### Lancement des tests Jacoco/PMD/Checkstyle :

Dans le dossier **"minihearthstone/server"** effectuer la commande `./gradlew clean build` et `./gradlew cobertura`.

Ensuite, accèder au fichier `build/report/[ PMD | checkstyle | tests ]/index.html` pour accèder au tests.

Pour Jacoco, lancer la commande `./gradlew jacocoTestReport` et accèder au fichier `build/jacocoHtml/index.html`.
>>>>>>> frontbackdev

